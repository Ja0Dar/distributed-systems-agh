name := "bookshop"

version := "0.1"

scalaVersion := "2.12.6"


val protocDependencies = Seq(
  "com.trueaccord.scalapb" %% "scalapb-runtime" % com.trueaccord.scalapb.compiler.Version.scalapbVersion % "protobuf"
)


resolvers += Resolver.bintrayRepo("beyondthelines", "maven")
val catsDependencies = Seq(
  "org.typelevel" %% "cats-core" % "1.1.0",
  "org.typelevel" % "mouse_2.12" % "0.17"
)

val logbackVersion = "1.2.3"
val slf4jVersion = "1.7.25"
val scalaLoggingVersion = "3.7.2"

val loggingDependencies = Seq(
  "ch.qos.logback" % "logback-classic" % logbackVersion,
  "ch.qos.logback" % "logback-core" % logbackVersion,
  "org.slf4j" % "jcl-over-slf4j" % slf4jVersion,
  "org.slf4j" % "log4j-over-slf4j" % slf4jVersion,
  "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingVersion
)

val akkaVersion = "2.5.9"

val akkaDependencies = Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-remote" % akkaVersion
)


val testDependencies = Seq(
  "org.scalatest" %% "scalatest" % "3.0.5" % Test,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
  "org.scalamock" %% "scalamock" % "4.1.0" % Test
)

libraryDependencies ++= List(
  catsDependencies,
  testDependencies,
  loggingDependencies,
  protocDependencies,
  akkaDependencies
).reduce(_ ++ _)


PB.protoSources in Compile := Seq(baseDirectory.value / "grpc")

PB.targets in Compile := Seq(
  scalapb.gen(flatPackage = true) -> (sourceManaged in Compile).value
)
