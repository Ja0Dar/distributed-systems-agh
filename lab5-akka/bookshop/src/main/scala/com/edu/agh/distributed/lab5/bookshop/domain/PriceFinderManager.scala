package com.edu.agh.distributed.lab5.bookshop.domain

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Status, Timers}
import com.edu.agh.distributed.lab5.bookshop.domain.PriceFinder.{PriceFinderRequest, PriceFinderResponse}
import com.edu.agh.distributed.lab5.bookshop.domain.PriceFinderManager.DbSearchTimeout
import com.edu.agh.distributed.lab5.client_interface.{GetBookPirce, GetBookPriceRespone}
import concurrent.duration._
import language.postfixOps

object PriceFinderManager {

  private case object DbSearchTimeout

}

class PriceFinderManager(clientId: String, databases: List[String]) extends Actor with Timers with ActorLogging {


  override def preStart(): Unit = timers.startSingleTimer("timeout", DbSearchTimeout, 90 seconds)

  override def receive: Receive = base

  def base: Receive = {
    {
      case GetBookPirce(title) =>
        databases.foreach {
          dbname =>
            val finder = context.actorOf(Props(new PriceFinder(dbname)))
            finder ! PriceFinderRequest(title)
        }

        context.become(waitingForFinderResponse(sender, title, databases.length))
    }
  }

  def waitingForFinderResponse(requester: ActorRef, requestedTitle: String, databasesThatShouldRespond: Int): Receive = {
    {
      case PriceFinderResponse(`requestedTitle`, Right(Some(entry))) =>
        requester ! GetBookPriceRespone(entry.title, Some(entry.price))
        context.stop(self)

      case PriceFinderResponse(`requestedTitle`, Right(None)) if databasesThatShouldRespond == 1 =>
        requester ! GetBookPriceRespone(requestedTitle, None)
        context.stop(self)

      case PriceFinderResponse(`requestedTitle`, Left(error)) if databasesThatShouldRespond == 1 =>
        log.warning(s"Database responded with error in client actor: $error")
        requester ! GetBookPriceRespone(requestedTitle, None)
        context.stop(self)

      case PriceFinderResponse(`requestedTitle`, Left(_) | Right(None)) =>
        context.become(waitingForFinderResponse(requester, requestedTitle, databasesThatShouldRespond - 1))

      case DbSearchTimeout =>
        log.error(s"Timeout reached while searching for $requestedTitle")
        requester ! GetBookPriceRespone(requestedTitle, None)
        context.stop(self)

    }: Receive
  } orElse commonHandler(s"waitingForFinderResponse(title=$requestedTitle,dbToRespond= $databasesThatShouldRespond)")


  private def commonHandler(state: String): Receive = {
    case other =>
      val message = s"ClientActor received unexpected message $other in state $state for id $clientId"
      log.warning(message)
      if (sender != self) sender ! Status.Failure(new IllegalStateException(message))
  }

}