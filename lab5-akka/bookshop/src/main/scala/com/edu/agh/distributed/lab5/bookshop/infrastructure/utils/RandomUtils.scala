package com.edu.agh.distributed.lab5.bookshop.infrastructure.utils

import scala.util.Random

object RandomUtils {
  def randomString = Random.alphanumeric.take(6).mkString
}
