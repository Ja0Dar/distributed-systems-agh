package com.edu.agh.distributed.lab5.bookshop.client

import akka.Done
import akka.actor.{Actor, ActorLogging, ActorRef, ActorSelection}
import com.edu.agh.distributed.lab5.client_interface._
import mouse.boolean._
import com.edu.agh.distributed.lab5.bookshop.infrastructure.utils.FancyImplicits._

class TerminalActor extends Actor with ActorLogging {
  var clientRef: Option[ActorRef] = None


  override def preStart(): Unit =
    bookShopActor() ! InitializeClient()


  override def receive: Receive = {
    case ClientInitialized(id) =>
      clientRef = Some(sender)
      println(s"Connected to server, clientId = $id")

    case GetBookPriceRespone(name, price) =>
      println(s"$name, price: ${price.map(_.toString).getOrElse("not found")}")

    case OrderBookResponse(title, success) =>
      println(s"Order for $title ${success.fold("succeeded", "failed")}")

    case StreamBookResponse(success) =>
      println(success.fold("Book will be streamed.", "Book won't be streamed."))

    case BookLine(number, line) =>
      println(s"$number : $line")

    case Done =>
      println("END of book excerpt.")

    case input: String =>
      commandFromInput(input) match {
        case Some(command) =>
          sendToClient(command)
        case None =>
          println("Not recognised command, use :")
          printPrompt()
      }


  }

  private def commandFromInput(input: String): Option[Any] = {
    val command = input.takeWhile(_ != ' ')
    val arg = input.replace(s"$command ", "")

    val pf: PartialFunction[String, Any] = {
      case "price" => GetBookPirce(arg)
      case "order" => OrderBook(arg)
      case "stream" => StreamBook(arg)
    }
    pf.lift(command)
  }

  @inline private def sendToClient(command: Any): Unit = clientRef match {
    case Some(ref) => ref ! command
    case None => log.error("Cannot send command, client not defined")
  }

  private def printPrompt(): Unit = List(
    "price <bookName>",
    "order <bookName",
    "stream <bookName>"
  ).foreach(println)


  private def bookShopActor(): ActorSelection = {
    val actorName = "bookshop"
    val host = "127.0.0.1"
    val port = 2551
    val actorSystem = "server"
    context.actorSelection(s"akka.tcp://$actorSystem@$host:$port/user/$actorName")
  }
}
