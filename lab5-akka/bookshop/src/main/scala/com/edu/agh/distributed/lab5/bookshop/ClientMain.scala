package com.edu.agh.distributed.lab5.bookshop

import java.io.{BufferedReader, InputStreamReader}

import akka.actor.{ActorSystem, Props}
import com.edu.agh.distributed.lab5.bookshop.client.TerminalActor
import com.typesafe.config.{ConfigFactory, ConfigValue, ConfigValueFactory}

import scala.annotation.tailrec
import scala.util.Random

object ClientMain extends App {

  val randomPort = 2550 + Random.nextInt(1000)

  val config = ConfigFactory.load("client.conf")
    .withValue("akka.remote.netty.tcp.port", ConfigValueFactory.fromAnyRef(randomPort))
  val system = ActorSystem("terminal", config)
  val terminalActor = system.actorOf(Props(new TerminalActor))

  val br = new BufferedReader(new InputStreamReader(System.in))
  var line = ""

  @tailrec
  def parseLine(): Unit = {
    line = br.readLine()
    if (List("q", "quit") contains line) {
      println("Exiting")
    } else {
      terminalActor ! line
      parseLine()
    }
  }

  parseLine()
  system.terminate()

}
