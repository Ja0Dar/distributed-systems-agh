package com.edu.agh.distributed.lab5.bookshop.infrastructure

import cats.implicits._
import com.edu.agh.distributed.lab5.bookshop.domain.Defs.{BookEntry, FindPriceError}
import com.edu.agh.distributed.lab5.bookshop.domain.Defs.FindPriceError.{DabataseNotFound, UnrecognisedLine}
import com.edu.agh.distributed.lab5.bookshop.infrastructure.utils.FancyImplicits._
import com.edu.agh.distributed.lab5.bookshop.infrastructure.utils.PrivmitiveConversionUtils._
import com.typesafe.scalalogging.StrictLogging

import scala.io.Source
import scala.language.postfixOps
import scala.util.Try

object DbReader extends StrictLogging {


  def findBookInDbByLowercaseTitle(dbFilePath: String, title: String): Either[FindPriceError, Option[BookEntry]] =
    findBookInDb(dbFilePath)(_.title.toLowerCase == title.toLowerCase)

  def findBookInDb(dbFile: String)(matcher: BookEntry => Boolean): Either[FindPriceError, Option[BookEntry]] =
    entriesFromDb(dbFile) map (_.find(matcher))

  private def entriesFromDb(dbfilePath: String): Either[FindPriceError, List[BookEntry]] =
    Try(Source.fromFile(dbfilePath))
      .toEither.left.map(_ => DabataseNotFound(dbfilePath))
      .flatMap(_
        .getLines()
        .map(_.split(";").toList)
        .toList
        .traverse(entryFromList).toRight(UnrecognisedLine(dbfilePath))
      )

  private def entryFromList: List[String] => Option[BookEntry] = {
    case title :: price :: Nil if isDouble(price) => Some(BookEntry(title, toDouble <| price))
    case other =>
      logger.warn(s"Couldn't recognise ${other.fold("")(_ + ";" + _)}")
      None
  }


}