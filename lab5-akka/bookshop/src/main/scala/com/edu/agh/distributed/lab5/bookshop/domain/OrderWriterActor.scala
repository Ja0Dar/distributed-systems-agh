package com.edu.agh.distributed.lab5.bookshop.domain

import akka.actor.{Actor, ActorLogging}
import com.edu.agh.distributed.lab5.bookshop.domain.OrderWriterActor._
import com.edu.agh.distributed.lab5.bookshop.infrastructure.OrderWriter

object OrderWriterActor {

  final case class WriteOrder(clientId: String, title: String)

  final case class WriteOrderResponse()

}

class OrderWriterActor extends Actor with ActorLogging {
  val orderWriter = new OrderWriter

  override def receive: Receive = {
    case WriteOrder(clientId, title) =>
      log.info(s"$clientId bought $title - writting to file")
      orderWriter.witeOrder(clientId, title)
      sender ! WriteOrderResponse()
  }
}
