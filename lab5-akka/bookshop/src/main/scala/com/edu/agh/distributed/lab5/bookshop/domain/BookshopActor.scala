package com.edu.agh.distributed.lab5.bookshop.domain

import akka.actor.{Actor, ActorLogging, ActorRef, OneForOneStrategy, Props, SupervisorStrategy}
import com.edu.agh.distributed.lab5.bookshop.infrastructure.utils.RandomUtils
import com.edu.agh.distributed.lab5.client_interface.InitializeClient
import concurrent.duration._
import language.postfixOps


class BookshopActor extends Actor with ActorLogging {
  private var orderWritter: ActorRef = _
  private val databases = "db/db1.txt" :: "db/db2.txt" :: Nil

  override def preStart(): Unit = {
    orderWritter = context.actorOf(Props(new OrderWriterActor))
  }

  override def receive: Receive = {
    case InitializeClient() =>
      val id = RandomUtils.randomString
      val client = context.actorOf(Props(new ClientActor(id, databases, orderWritter)))
      client.forward(InitializeClient())
      log.debug(s"Created client actor with id $id")
  }

  override def supervisorStrategy: SupervisorStrategy =
    OneForOneStrategy(10, 10 seconds, loggingEnabled = true) {
      case _: Exception => SupervisorStrategy.restart
    }
}
