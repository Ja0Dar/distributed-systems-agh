package com.edu.agh.distributed.lab5.bookshop.infrastructure.utils

object FancyImplicits {

  implicit class ExtFun[A, B](f: A => B) {
    def <| : A => B = f.apply
  }

}
