package com.edu.agh.distributed.lab5.bookshop.domain

import akka.Done
import akka.actor.ActorRef
import akka.stream.{ActorMaterializer, ThrottleMode}
import akka.stream.scaladsl.{Sink, Source}
import com.edu.agh.distributed.lab5.client_interface.{BookLine, StreamBookResponse}

import scala.concurrent.duration._
import scala.io.{Source => IOSource}
import scala.language.postfixOps
import scala.util.Try


object BookStreamer {
  val directoryPrefix = "books"

  def streamBook(title: String, ref: ActorRef)(implicit mat: ActorMaterializer) = {
    val filename = s"$directoryPrefix/${title.trim.replace(" ", "_").toLowerCase}.txt"

    val file = Try(IOSource.fromFile(filename)).toOption

    ref ! StreamBookResponse(file.isDefined)

    file.foreach { bufferedSource =>
      Source
        .fromIterator(bufferedSource.getLines)
        .throttle(
          elements = 1,
          per = 1 second,
          maximumBurst = 1,
          mode = ThrottleMode.shaping
        ).zipWithIndex
        .map { case (line, index) => BookLine(index, line) }
        .runWith(Sink.actorRef(ref, Done))
    }
  }
}
