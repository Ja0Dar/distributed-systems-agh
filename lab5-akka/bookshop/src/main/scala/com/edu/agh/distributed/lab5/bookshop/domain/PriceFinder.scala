package com.edu.agh.distributed.lab5.bookshop.domain

import akka.actor.Actor
import com.edu.agh.distributed.lab5.bookshop.domain.Defs.{BookEntry, FindPriceError}
import com.edu.agh.distributed.lab5.bookshop.domain.PriceFinder.{PriceFinderRequest, PriceFinderResponse}
import com.edu.agh.distributed.lab5.bookshop.infrastructure.DbReader

object PriceFinder {

  final case class PriceFinderRequest(title: String)

  final case class PriceFinderResponse(requestedTitle: String, result: Either[FindPriceError, Option[BookEntry]])

}

class PriceFinder(databaseName: String) extends Actor {
  override def receive: Receive = {
    case PriceFinderRequest(title) =>
      sender ! PriceFinderResponse(title, DbReader.findBookInDbByLowercaseTitle(databaseName, title))
      context.stop(self)
  }
}
