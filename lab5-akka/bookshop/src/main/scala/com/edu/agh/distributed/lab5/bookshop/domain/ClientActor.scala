package com.edu.agh.distributed.lab5.bookshop.domain

import akka.actor.{Actor, ActorLogging, ActorRef, AllForOneStrategy, OneForOneStrategy, Props, Status, SupervisorStrategy}
import akka.pattern.{ask, pipe}
import akka.stream.ActorMaterializer
import akka.util.Timeout
import cats.implicits._
import cats.instances.future

import scala.concurrent.ExecutionContext.Implicits.global
import com.edu.agh.distributed.lab5.bookshop.domain.OrderWriterActor.{WriteOrder, WriteOrderResponse}
import com.edu.agh.distributed.lab5.bookshop.domain.PriceFinder.PriceFinderResponse
import com.edu.agh.distributed.lab5.client_interface._

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps

class ClientActor(id: String, databases: List[String], orderWriter: ActorRef) extends Actor with ActorLogging {

  private implicit val timeout: Timeout = Timeout(10 seconds)
  implicit val mat: ActorMaterializer = ActorMaterializer()

  override def receive: Receive = created

  def created: Receive = {
    {
      case InitializeClient() =>
        sender ! ClientInitialized(id)
        context.become(initialized)
    }: Receive
  } orElse commonHandler("created")


  def initialized: Receive = {
    {
      case command: GetBookPirce =>
        newFinderManager() forward command

      case OrderBook(title) =>
        (newFinderManager() ? GetBookPirce(title))
          .mapTo[GetBookPriceRespone]
          .flatMap { entry =>
            entry.price.traverse[Future, WriteOrderResponse](_ =>
              (orderWriter ? WriteOrder(id, entry.name))
                .mapTo[WriteOrderResponse]
            )
              .map {
                case Some(WriteOrderResponse()) => OrderBookResponse(entry.name, success = true)
                case None => OrderBookResponse(entry.name, success = false)
              }
          }.pipeTo(sender)


      case StreamBook(title) =>
        BookStreamer.streamBook(title, sender)


      case PriceFinderResponse(_, _) => //ignore
    }: Receive
  } orElse commonHandler("Initialized")


  private def commonHandler(state: String): Receive = {
    case other =>
      val message = s"ClientActor received unexpected message $other in state $state for id $id"
      log.warning(message)
      if (sender != self) sender ! Status.Failure(new IllegalStateException(message))
  }

  private def newFinderManager() = context.actorOf(Props(new PriceFinderManager(id, databases)))

  override def supervisorStrategy: SupervisorStrategy =
    AllForOneStrategy(10, 10 seconds, loggingEnabled = true) {
      case _: Exception => SupervisorStrategy.escalate
    }

}
