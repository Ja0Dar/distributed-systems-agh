package com.edu.agh.distributed.lab5.bookshop.domain

object Defs {
  type Money = Double

  case class BookEntry(title: String, price: Money)

  sealed trait FindPriceError

  object FindPriceError {

    final case class DabataseNotFound(databaseName: String) extends FindPriceError

    final case class UnrecognisedLine(databaseName: String) extends FindPriceError

  }

}
