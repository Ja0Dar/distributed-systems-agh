package com.edu.agh.distributed.lab5.bookshop.infrastructure.utils

object PrivmitiveConversionUtils {
  def isDouble(string: String) = {
    string.filter(_ == '.').length <= 1 && string.filterNot(_ == '.').forall(_.isDigit)
  }

  def toDouble: String => Double = java.lang.Double.parseDouble
}
