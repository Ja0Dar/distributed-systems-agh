package com.edu.agh.distributed.lab5.bookshop

import akka.actor.{ActorSystem, Props}
import com.edu.agh.distributed.lab5.bookshop.domain.BookshopActor
import com.typesafe.config.ConfigFactory


object ServerMain extends App {

  val system = ActorSystem("server", ConfigFactory.load("server.conf"))

  val bookshop = system.actorOf(Props(new BookshopActor), "bookshop")
}
