package com.edu.agh.distributed.lab5.bookshop.infrastructure

import java.io.FileWriter

import com.typesafe.scalalogging.StrictLogging

import scala.language.postfixOps

class OrderWriter extends StrictLogging {
  val fw = new FileWriter("orders.txt", true)

  def witeOrder(clienId: String, bookTitle: String): Unit = {
    if (fw == null) {
      logger.error("Cannot open file writer. too bad")
    }
    fw.append(s"$clienId bought $bookTitle\n")
    fw.flush()
  }


}