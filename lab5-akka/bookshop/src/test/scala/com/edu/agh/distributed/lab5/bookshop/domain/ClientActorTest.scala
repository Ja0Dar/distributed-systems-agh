package com.edu.agh.distributed.lab5.bookshop.domain

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import com.edu.agh.distributed.lab5.bookshop.domain.OrderWriterActor.{WriteOrder, WriteOrderResponse}
import com.edu.agh.distributed.lab5.bookshop.infrastructure.utils.RandomUtils
import com.edu.agh.distributed.lab5.client_interface._
import com.typesafe.config.ConfigFactory
import org.scalatest.FlatSpecLike

class ClientActorTest extends TestKit(ActorSystem("clientActorTest", ConfigFactory.load("server.conf"))) with FlatSpecLike with ImplicitSender {

  "InitializeClient" should "produce respone with client id" in {
    val id = RandomUtils.randomString
    val clientRef = system.actorOf(Props(new ClientActor(id, Nil, ActorRef.noSender)))

    clientRef ! InitializeClient()
    expectMsg(ClientInitialized(id))
  }

  "GetBookPrice" should "return correct book price" in {
    val id = RandomUtils.randomString
    val clientRef = system.actorOf(Props(new ClientActor(id, "db/db1.txt" :: Nil, ActorRef.noSender)))

    clientRef ! InitializeClient()
    expectMsg(ClientInitialized(id))

    clientRef ! GetBookPirce("carrie")

    expectMsg(GetBookPriceRespone("Carrie", Some(66.66)))
  }

  it should "return price = None if book doesn't exist in any db" in {
    val id = RandomUtils.randomString
    val clientRef = system.actorOf(Props(new ClientActor(id, "db/db1.txt" :: Nil, ActorRef.noSender)))

    clientRef ! InitializeClient()
    expectMsg(ClientInitialized(id))

    clientRef ! GetBookPirce("curry")

    expectMsg(GetBookPriceRespone("curry", None))
  }

  it should "correctly handle errors if db is not available" in {
    val id = RandomUtils.randomString
    val clientRef = system.actorOf(Props(new ClientActor(id, "wrongDbName" :: Nil, ActorRef.noSender)))

    clientRef ! InitializeClient()
    expectMsg(ClientInitialized(id))

    clientRef ! GetBookPirce("curry")

    expectMsg(GetBookPriceRespone("curry", None))
  }


  "OrderBook" should "result in success if book exists" in {
    val id = RandomUtils.randomString
    val probe = TestProbe()
    val clientRef = system.actorOf(Props(new ClientActor(id, "db/db1.txt" :: Nil, probe.ref)))

    clientRef ! InitializeClient()
    expectMsg(ClientInitialized(id))

    clientRef ! OrderBook("carrie")
    probe.expectMsg(WriteOrder(id, "Carrie"))
    probe.reply(WriteOrderResponse())

    expectMsg(OrderBookResponse("Carrie", success = true))

  }

}
