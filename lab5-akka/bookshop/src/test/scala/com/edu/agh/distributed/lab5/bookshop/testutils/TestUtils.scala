package com.edu.agh.distributed.lab5.bookshop.testutils

import scala.util.Random

object TestUtils {
  def randomString = Random.nextString(6)
}
