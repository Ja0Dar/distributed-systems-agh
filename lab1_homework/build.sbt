name := "lab1_homework"

version := "0.1"

scalaVersion := "2.12.4"

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.8.0"