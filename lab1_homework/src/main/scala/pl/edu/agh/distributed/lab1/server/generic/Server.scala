package pl.edu.agh.distributed.lab1.server.generic

import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.locks.ReentrantLock

import com.typesafe.scalalogging.StrictLogging
import pl.edu.agh.distributed.lab1.common.{Client, Utils}
import pl.edu.agh.distributed.lab1.server.generic.Server.BroadcastStrategy

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object Server {

  abstract class BroadcastStrategy {
    def apply(broadcastOperation: => Unit): Unit
  }

  val asyncBroadcast: BroadcastStrategy = new BroadcastStrategy {
    override def apply(broadcastOperation: => Unit): Unit = broadcastOperation
  }

  val syncBroadcast: BroadcastStrategy = new BroadcastStrategy {
    val lock = new ReentrantLock()

    override def apply(broadcastOperation: => Unit): Unit = {
      lock.lock()
      broadcastOperation
      lock.unlock()
    }
  }
}

abstract class Server[Message, C <: Client[Message]](val serverType: String, broadcastStrategy: BroadcastStrategy) extends Utils with StrictLogging {
  println(s"Scala $serverType server")

  def asyncListenForClients: Int => Future[Unit] = port => Future(listenForClients(port))

  def listenForClients(port: Int): Unit

  protected def preparedMessage(sender: C, msg: Message): Message

  protected def greetingToMessage(greeting: String): Message


  protected def broadcastMessage(sender: C, msg: Message): Unit = broadcastStrategy(_broadcastMessage(sender, msg))

  protected val clients: mutable.Buffer[C] = new CopyOnWriteArrayList[C]().asScala

  protected def subscribeClient(client: C, receiveEndCallback: C => Unit = _ => Unit): Future[Unit] = Future {
    greetClient(client)
    clients += client
    logger.info(s"${client.nick} joined $serverType chat, people in chat : ${clients.length}")

    repeatUntilNone(client.receive(),
      broadcastMessage(client, _: Message),
      {
        receiveEndCallback(client)
        logger.warn(s"Finished listening to ${client.nick} on $serverType")
      }
    )

    clients -= client
    logger.info(s"${client.nick} left $serverType chat, people in chat : ${clients.length}")
  }


  protected def greetClient(client: C): Unit = {
    val greeting = s"Hello ${client.nick}.\n" + (if (clients.nonEmpty) s"Currently in chat there are: ${
      clients map (_.nick) filter (_ != client.nick) reduce (_ + ", " + _)
    }." else "Currently chat is empty.")
    client.send(greetingToMessage(greeting))
  }

  protected def _broadcastMessage(sender: C, msg: Message): Unit =
    clients.filter(_.nick != sender.nick).toParArray.foreach(_.send(preparedMessage(sender, msg)))
}