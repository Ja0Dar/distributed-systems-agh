package pl.edu.agh.distributed.lab1.common

import scala.annotation.tailrec

trait Utils {
  @tailrec
  final def repeatUntilNone[T, R](optGenerator: => Option[T], someAction: T => Unit, noneAction: => R): R =
    optGenerator match {
      case Some(t) =>
        someAction(t)
        repeatUntilNone(optGenerator, someAction, noneAction)
      case None => noneAction
    }

}
