package pl.edu.agh.distributed.lab1.server

import pl.edu.agh.distributed.lab1.server.specific.{TcpServer, UdpServer}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.language.postfixOps

object ServerMain extends App {
  val servers = UdpServer.asyncBroadcastServer :: TcpServer.asyncBroadcastServer :: Nil
  val serverPort = 9999

  val fut = Future.sequence(servers.map(_.asyncListenForClients(serverPort)))

  Await.ready(fut, 1 day)
}
