package pl.edu.agh.distributed.lab1.client.specific

import java.net.InetAddress

import com.typesafe.scalalogging.StrictLogging
import pl.edu.agh.distributed.lab1.client.generic.ClientService
import pl.edu.agh.distributed.lab1.common.Def.Port
import pl.edu.agh.distributed.lab1.common.{MulticastClient, Utils}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Try

object MulticastClientService extends ClientService[String, MulticastClient] with StrictLogging with Utils {
  def connect(nickname: String, host: InetAddress, multicastListenPort: Int): Try[MulticastClient] =
    Try {
      val multicastPort = Port.withRandomSend(multicastListenPort)
      val client = new MulticastClient(nickname, host, multicastPort)
      recieveContinuosly(client)
      client
    }

  def recieveContinuosly(client: MulticastClient) =
    Future {
      repeatUntilNone(client.receive(), println, println("Finishing reading form server"))
    }
}
