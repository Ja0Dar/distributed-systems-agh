package pl.edu.agh.distributed.lab1.common

import java.net.URLEncoder

import scala.io.Source
import scala.util.Try

object AsciiArtProvider {
  val url = "http://artii.herokuapp.com/make?text="

  def blockingArtify(text: String): Try[String] = Try {
    val address = s"$url${URLEncoder.encode(text, "utf-8")}"
    Source.fromURL(address).mkString
  }

  val failedMessage = "  __  __                                 ______    _ _          _ \n |  \\/  |                               |  ____|  (_) |        | |\n | \\  / | ___ ___ ___  __ _  __ _  ___  | |__ __ _ _| | ___  __| |\n | |\\/| |/ _ \\ __/ __|/ _` |/ _` |/ _ \\ |  __/ _` | | |/ _ \\/ _` |\n | |  | |  __\\__ \\__ \\ (_| | (_| |  __/ | | | (_| | | |  __/ (_| |\n |_|  |_|\\___|___/___/\\__,_|\\__, |\\___| |_|  \\__,_|_|_|\\___|\\__,_|\n                             __/ |                                \n                            |___/                                 "
}
