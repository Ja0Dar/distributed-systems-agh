package pl.edu.agh.distributed.lab1.client.specific

import java.io._
import java.net.{InetAddress, Socket}

import com.typesafe.scalalogging.StrictLogging
import pl.edu.agh.distributed.lab1.client.generic.ClientService
import pl.edu.agh.distributed.lab1.common.{TcpClient, Utils}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Promise}
import scala.util.{Success, Try}

object TcpClientService extends ClientService[String, TcpClient] with StrictLogging with Utils {
  def connect(nickname: String, host: InetAddress, serverListenPort: Int): Try[TcpClient] =
    Try {
      val socket = new Socket(host, serverListenPort)
      val out = new PrintWriter(socket.getOutputStream, true)
      val in = new BufferedReader(new InputStreamReader(socket.getInputStream))
      out.println(nickname)

      val promise = Promise[Unit]
      val client = new TcpClient(nickname, socket, in, out, promise.future)

      printClientsMessages(client, promise)
      client
    }


  private def printClientsMessages(client: TcpClient, promise: Promise[Unit]): Unit = // todo change name
    Future {
      repeatUntilNone[String, Unit](
        client.receive(),
        println,
        promise.complete(Success(println("Finishing reading form server")))
      )
    }
}
