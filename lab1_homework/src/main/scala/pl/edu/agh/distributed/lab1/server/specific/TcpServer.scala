package pl.edu.agh.distributed.lab1.server.specific

import java.io.{BufferedReader, InputStreamReader, PrintWriter}
import java.net.ServerSocket

import pl.edu.agh.distributed.lab1.common.TcpClient
import pl.edu.agh.distributed.lab1.server.generic.Server.BroadcastStrategy
import pl.edu.agh.distributed.lab1.server.generic.{Server, StringSendingServer}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Promise
import scala.util.Success

object TcpServer extends {
  def asyncBroadcastServer: TcpServer = new TcpServer(Server.asyncBroadcast)

  def syncBroadcastServer: TcpServer = new TcpServer(Server.syncBroadcast)
}

// todo - unsubscribe if future completes
class TcpServer(broadcastStrategy: BroadcastStrategy)
  extends StringSendingServer[TcpClient]("TCP", broadcastStrategy) {

  override def listenForClients(port: Int): Unit = {
    val socket = new ServerSocket(port)

    while (true) {
      val clientSocket = socket.accept()
      val out = new PrintWriter(clientSocket.getOutputStream, true)
      val in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream))
      val nick = in.readLine()

      if (nick != null && !clients.exists(_.nick == nick)) {
        val promise = Promise[Unit]
        subscribeClient(new TcpClient(nick, clientSocket, in, out, promise.future))
          .map(_ => promise.complete(Success(Unit)))
      }
      else {
        rejectClient(out, s"Cannot add $nick. Nick already in use.")
        clientSocket.close()
      }
    }
  }

  private def rejectClient(out: PrintWriter, message: String): Unit = {
    logger.warn(message)
    out.println(message)
  }
}
