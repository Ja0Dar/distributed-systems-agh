package pl.edu.agh.distributed.lab1.server.specific

import java.net.{DatagramPacket, DatagramSocket, InetAddress}

import pl.edu.agh.distributed.lab1.common.Def.Port
import pl.edu.agh.distributed.lab1.common.UdpClient
import pl.edu.agh.distributed.lab1.server.generic.Server.BroadcastStrategy
import pl.edu.agh.distributed.lab1.server.generic.{Server, StringSendingServer}

object UdpServer {
  def asyncBroadcastServer: UdpServer = new UdpServer(Server.asyncBroadcast)

  def syncBroadcastServer: UdpServer = new UdpServer(Server.syncBroadcast)
}

class UdpServer(broadcastStrategy: BroadcastStrategy)
  extends StringSendingServer[UdpClient]("Udp", broadcastStrategy) {
  //TODO - in case of boredom - implement keepalives?

  override protected def greetClient(client: UdpClient): Unit = () // Not useful if TCP, udp are used together.

  override def listenForClients(serverListenPort: Int): Unit = {
    val serverPort = Port.fromListen(serverListenPort)

    val receiveSocket = new DatagramSocket(serverPort.receive)
    while (true) {
      val buffer = new Array[Byte](1024)
      val receivePacket = new DatagramPacket(buffer, buffer.length)
      receiveSocket.receive(receivePacket)

      val clientAddr = receivePacket.getAddress
      val clientPort = Port.fromSend(receivePacket.getPort)
      val msg = new String(receivePacket.getData.filter(_ != 0))


      logger.debug(s"$clientAddr,$clientPort,$msg")

      clients.find(c => c.addr == clientAddr && c.remotePort == clientPort) match {

        case Some(client) => client.offer(msg)

        case None if msg != null && !clients.exists(_.nick == msg) =>
          subscribeClient(new UdpClient(msg, clientAddr, Port.withRandomSend(serverListenPort), clientPort))

        case None =>
          rejectClient(serverPort, clientAddr, clientPort, s"Cannot add $msg. Nick already in use.")
      }
    }
  }

  private def rejectClient(serverPort: Port, addr: InetAddress, clientPort: Port, msg: String): Unit = {
    logger.warn(msg)
    val datagramSocket = new DatagramSocket(serverPort.send)
    datagramSocket.send(new DatagramPacket(msg.getBytes, msg.getBytes.length, addr, clientPort.receive))
    datagramSocket.close()
  }
}
