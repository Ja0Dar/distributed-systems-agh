package pl.edu.agh.distributed.lab1.client.specific

import java.net.{DatagramPacket, DatagramSocket, InetAddress}

import com.typesafe.scalalogging.StrictLogging
import pl.edu.agh.distributed.lab1.client.generic.ClientService
import pl.edu.agh.distributed.lab1.common.Def.Port
import pl.edu.agh.distributed.lab1.common.{UdpClient, Utils}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Try

//TODO - didn't receive Rejection,
// TODO - ports are always changing
object UdpClientService extends ClientService[String, UdpClient] with StrictLogging with Utils {
  def connect(nickname: String, host: InetAddress, serverListenPort: Int): Try[UdpClient] =
    Try {
      val localPort = Port.randomPort
      val client = new UdpClient(nickname, host, localPort, Port.fromListen(serverListenPort))
      recieveContinuosly(client)
      client.send(client.nick)
      client
    }

  def recieveContinuosly(client: UdpClient) = {
    val socket = new DatagramSocket(client.localPort.receive)
    Future {
      repeatUntilNone(receiveMsg(socket), println, println("Finishing reading form server"))
    }
  }

  def receiveMsg(socket: DatagramSocket): Option[String] = {
    val buffer = new Array[Byte](1024)
    val receivePacket = new DatagramPacket(buffer, buffer.length)
    socket.receive(receivePacket)
    Option(new String(receivePacket.getData.filter(_ != 0)))
  }

}
