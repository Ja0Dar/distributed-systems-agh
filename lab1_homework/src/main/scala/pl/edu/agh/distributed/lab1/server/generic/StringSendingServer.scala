package pl.edu.agh.distributed.lab1.server.generic

import pl.edu.agh.distributed.lab1.common.Client
import pl.edu.agh.distributed.lab1.server.generic.Server.BroadcastStrategy

abstract class StringSendingServer[C <: Client[String]](serverType: String, broadcastStrategy: BroadcastStrategy)
  extends Server[String, C](serverType, broadcastStrategy) {
  override protected def greetingToMessage(greeting: String): String = greeting

  override protected def preparedMessage(sender: C, msg: String): String = s"${sender.nick} >>>\n $msg"

}
