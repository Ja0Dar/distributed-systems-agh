package pl.edu.agh.distributed.lab1.common

import scala.util.Random

object Def extends Utils {

  object Port {
    def fromListen(listenPort: Int): Port = new Port(listenPort)

    def fromSend(sendPort: Int): Port = new Port(sendPort - 1)

    def withRandomSend(listenPort: Int): Port = new Port(listenPort) {
      override def send: Int = randomPortNumber
    }

    def randomPort = new Port(randomPortNumber)

    private def randomPortNumber = 1024 + math.abs(Random.nextInt) % 8000
  }

  class Port(listenPort: Int) {
    def receive: Int = listenPort

    def send: Int = listenPort + 1

    override def hashCode(): Int = listenPort.hashCode()

    override def equals(o: scala.Any): Boolean = o.isInstanceOf[Port] && o.asInstanceOf[Port].receive == receive

    override def toString: String = s"Port(receive :$receive,send: $send)"
  }

}
