package pl.edu.agh.distributed.lab1.client.generic

import java.net.InetAddress

import com.typesafe.scalalogging.StrictLogging
import pl.edu.agh.distributed.lab1.common.{Client, Utils}

import scala.util.Try

trait ClientService[Message, C <: Client[Message]] extends StrictLogging with Utils {
  def connect(nickname: String, host: InetAddress, port: Int): Try[C]
}
