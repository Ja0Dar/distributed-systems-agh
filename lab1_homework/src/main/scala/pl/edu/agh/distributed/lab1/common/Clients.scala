package pl.edu.agh.distributed.lab1.common

import java.io.{BufferedReader, PrintWriter}
import java.net._
import java.util.concurrent.LinkedBlockingQueue

import com.sun.istack.internal.Nullable
import pl.edu.agh.distributed.lab1.common.Def.Port

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, blocking}


sealed abstract class Client[Message](val nick: String) extends AutoCloseable {

  def send(msg: Message): Unit = blocking(_send(msg))

  def receive(): Option[Message] = Option(blocking(_receive()))

  protected def _send(msg: Message): Unit

  protected def _receive(): Message

  def close(): Unit
}

final class TcpClient(nick: String, val socket: Socket, incoming: BufferedReader, outgoing: PrintWriter, val listeningFuture: Future[Unit]) extends Client[String](nick) with Utils {

  @Nullable
  def _receive(): String = incoming.readLine

  def _send(msg: String): Unit = outgoing println msg

  override def close(): Unit = socket.close()
}


final class UdpClient(nick: String, val addr: InetAddress, val localPort: Port, val remotePort: Port) extends Client[String](nick) {
  val queue = new LinkedBlockingQueue[String]()
  val socket = new DatagramSocket(localPort.send)

  def offer(msg: String) = Future(blocking(queue.put(msg)))

  def _receive(): String = queue.take()

  def _send(msg: String): Unit = {
    val msgBytes = msg.getBytes()
    val packet = new DatagramPacket(msgBytes, msgBytes.length, addr, remotePort.receive)
    socket.send(packet)
  }

  override def close(): Unit = socket.close()
}

final class MulticastClient(nick: String, val addr: InetAddress, mutlicastPort: Port) extends Client[String](nick) {
  val receiveSocket = new MulticastSocket(mutlicastPort.receive)
  receiveSocket.joinGroup(addr)

  val sendSocket = new DatagramSocket(mutlicastPort.send)

  @Nullable
  def _receive(): String = {
    val buffer = new Array[Byte](1024)
    val receivePacket = new DatagramPacket(buffer, buffer.length)
    receiveSocket.receive(receivePacket)
    new String(receivePacket.getData.filter(_ != 0))
  }

  def _send(msg: String): Unit = {
    val msgBytes = msg.getBytes()
    val packet = new DatagramPacket(msgBytes, msgBytes.length, addr, mutlicastPort.receive)
    sendSocket.send(packet)
  }

  override def close(): Unit = receiveSocket.close()
}




