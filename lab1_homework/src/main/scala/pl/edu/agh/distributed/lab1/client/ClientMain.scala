package pl.edu.agh.distributed.lab1.client

import java.net.InetAddress

import pl.edu.agh.distributed.lab1.client.specific.{MulticastClientService, TcpClientService, UdpClientService}
import pl.edu.agh.distributed.lab1.common.{AsciiArtProvider, MulticastClient}

import scala.io.StdIn

object ClientMain extends App {

  println("Nick:")
  val nick = StdIn.readLine()

  val asciiMsg = AsciiArtProvider.blockingArtify(nick).getOrElse(AsciiArtProvider.failedMessage)

  val serverAddr = InetAddress.getLocalHost
  //  val serverAddr = InetAddress.getByName(StdIn.readLine())
  val serverPort = 9999
  val multicastAddr = InetAddress.getByName("230.1.1.1")
  val multicastPort = 1500


  val clients =
    for {
      tcpClient <- TcpClientService.connect(nick, serverAddr, serverPort)
      udpClient <- UdpClientService.connect(nick, serverAddr, serverPort)
      multiClient <- MulticastClientService.connect(nick, multicastAddr, multicastPort)
    } yield (tcpClient, udpClient, multiClient)

  clients.map { case (tcp, udp, multicast) =>
    while (true) {
      StdIn.readLine match {
        case "U" => udp.send(asciiMsg)

        case "M" => multicast.send(asciiMsg)

        case message if !tcp.listeningFuture.isCompleted =>
          tcp.send(message)

        case _ => multicastRecovery(multicast)
      }
    }
  }.map(_ => println("Finished gracefully")).recoverWith { case _ =>
    MulticastClientService.connect(nick, multicastAddr, multicastPort).map(multicastRecovery)
  }

  def multicastRecovery(multi: MulticastClient): Unit = {
    println("Connecting to server failed. Trying to use only multicast. M")
    while (true) {
      StdIn.readLine match {
        case "M" => multi.send(asciiMsg)
        case message => multi.send(s"$nick via multicast >>>\n $message")
      }
    }
  }

}