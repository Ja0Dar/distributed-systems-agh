package pl.edu.agh.distributed.lab2;

import lombok.val;

import java.util.Arrays;
import java.util.List;

public class Controller {
    private final List<String> putAliases = Arrays.asList("put", "add");
    private final List<String> containsAliases = Arrays.asList("contains", "c");
    private final List<String> showAliases = Arrays.asList("show", "ls", "list");
    private final List<String> removeAliases = Arrays.asList("remove", "rm", "r");

    private MyHashMap map;

    private String prompt = new StringBuilder()
            .append(join(putAliases, "|"))
            .append(" <key> <value>\n")
            .append(join(removeAliases, "|"))
            .append(" <key>\n")
            .append(join(showAliases, "|"))
            .append("\n")
            .append(join(containsAliases, "|"))
            .append(" <key>").toString();

    public Controller(MyHashMap map) {
        this.map = map;
    }

    public void parseArg(final String arg) {
        if (isContains(arg)) {
            System.out.println(contains(arg));
        } else if (isShow(arg)) {
            show();
        } else if (isPut(arg)) {
            put(arg);
            System.out.println("Inserted");
        } else if (isRemove(arg)) {
            remove(arg);
            System.out.println("Removed");
        } else {
            System.out.println(prompt);
        }
    }

    private boolean isContains(String contains) {
        val tokens = contains.split(" ");
        return tokens.length == 2 && containsAliases.contains(tokens[0]);
    }

    private boolean contains(String contains) {
        val tokens = contains.split(" ");
        return map.containsKey(tokens[1]);
    }

    private boolean isShow(String show) {
        return showAliases.contains(show);
    }

    private void show() {
        System.out.println(map);
    }

    private boolean isPut(String put) {
        val tokens = put.split(" ");
        return tokens.length == 3 && putAliases.contains(tokens[0]);
    }

    private void put(String put) {
        val tokens = put.split(" ");
        map.put(tokens[1], tokens[2]);
    }

    private boolean isRemove(String remove) {
        val tokens = remove.split(" ");
        return tokens.length == 2 && removeAliases.contains(tokens[0]);
    }

    private void remove(String remove) {
        val tokens = remove.split(" ");
        map.remove(tokens[1]);
    }

    private String join(List<String> list, String separator) {
        if (list.isEmpty()) {
            return "";
        }
        return list.stream().reduce((a, b) -> a + separator + b).orElse(list.get(0));
    }

}
