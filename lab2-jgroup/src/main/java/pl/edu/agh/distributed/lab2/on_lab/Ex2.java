package pl.edu.agh.distributed.lab2.on_lab;

import org.jgroups.JChannel;
import org.jgroups.Message;
import org.jgroups.protocols.*;
import org.jgroups.protocols.pbcast.FLUSH;
import org.jgroups.protocols.pbcast.GMS;
import org.jgroups.protocols.pbcast.NAKACK2;
import org.jgroups.protocols.pbcast.STABLE;
import org.jgroups.stack.ProtocolStack;
import pl.edu.agh.distributed.lab2.protos.Bank;

class Ex2 {

    private void start() {
    }

    public static void main(String[] args) throws Exception {
        System.setProperty("java.net.preferIPv4Stack", "true");

        byte[] add = Bank.BankOperation.newBuilder().setType(Bank.BankOperation.OperationType.ADD).setValue(2).build().toByteArray();
        byte[] multiply = Bank.BankOperation.newBuilder().setType(Bank.BankOperation.OperationType.MULTIPLY).setValue(3).build().toByteArray();

        JChannel channel = new JChannel(false);
        ProtocolStack stack = new ProtocolStack();
        channel.setProtocolStack(stack);
        stack.addProtocol(new UDP()) // todo : new UDP().setValue("mcast_group_addr",InetAddress.getByName("230.0.0.x"))
                .addProtocol(new PING())
                .addProtocol(new MERGE3())
                .addProtocol(new FD_SOCK())
                .addProtocol(new FD_ALL().setValue("timeout", 12000).setValue("interval", 3000))
                .addProtocol(new VERIFY_SUSPECT())
                .addProtocol(new BARRIER())
                .addProtocol(new NAKACK2())
                .addProtocol(new UNICAST3())
                .addProtocol(new STABLE())
                .addProtocol(new GMS())
                .addProtocol(new UFC())
                .addProtocol(new MFC())
                .addProtocol(new FRAG2())
                .addProtocol(new SEQUENCER())// provides
                .addProtocol(new FLUSH());

        stack.init();


        channel.connect("operation");


        for (int i = 0; i < 2000; i++) {
            Message msg = new Message(null, null, add);
            channel.send(msg);
        }

        Thread.sleep(5 * 60 * 1000);
        channel.close();

        //Zad - rozproszona tablica hashujaca
        //ma byc dostepna  i ma miec eventual consistency
    }
}

