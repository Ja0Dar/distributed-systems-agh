package pl.edu.agh.distributed.lab2.on_lab;

import pl.edu.agh.distributed.lab2.protos.Bank;

import java.io.IOException;
import java.net.*;

public class Ex1 {
    public static void main(String[] args) throws IOException, InterruptedException {
        int port = 123;

        DatagramSocket sendSocket = new DatagramSocket(1234);

        InetAddress address = Inet4Address.getByName("224.0.0.7");

        byte[] add = Bank.BankOperation.newBuilder().setType(Bank.BankOperation.OperationType.ADD).setValue(2).build().toByteArray();
        byte[] multiply = Bank.BankOperation.newBuilder().setType(Bank.BankOperation.OperationType.MULTIPLY).setValue(3).build().toByteArray();



        int max = 2000;

        for (int j = 0; j <max; j++) {

            sendSocket.send(new DatagramPacket(add, add.length, address, 6789));

            Thread.sleep((long) (Math.random() * 1));
            System.out.println(j);
        }

    }
}
