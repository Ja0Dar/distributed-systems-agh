package pl.edu.agh.distributed.lab2;

import lombok.val;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class ProtoHelpers {
    public static MapCommands.Request putRequest(final String key, final String value) {
        return MapCommands.Request.newBuilder().setPut(MapCommands.PutRequest.newBuilder().setKey(key).setValue(value).build()).build();
    }

    public static MapCommands.Request removeRequest(final String key) {
        return MapCommands.Request.newBuilder().setRemove(MapCommands.RemoveRequest.newBuilder().setKey(key).build()).build();
    }


    public static MapCommands.Map mapToProto(Map<String, String> map) {
        val entries = map.entrySet().stream()
                .map(entry -> protoMapEntry(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());

        return MapCommands.Map.newBuilder().addAllEntries(entries).build();
    }

    public static ConcurrentHashMap<String, String> protoMapToDomain(MapCommands.Map protoMap) {
        val result = new ConcurrentHashMap<String, String>();
        protoMap.getEntriesList()
                .forEach(mapEntry ->
                        result.put(mapEntry.getKey(),
                                mapEntry.getValue()
                        ));

        return result;
    }

    private static MapCommands.Map.MapEntry protoMapEntry(String key, String value) {
        return MapCommands.Map.MapEntry.newBuilder().setKey(key).setValue(value).build();
    }
}
