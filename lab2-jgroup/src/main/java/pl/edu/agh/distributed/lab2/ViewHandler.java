package pl.edu.agh.distributed.lab2;

import lombok.val;
import org.jgroups.JChannel;
import org.jgroups.MergeView;

class ViewHandler extends Thread {
    private JChannel ch;
    private MergeView view;

    ViewHandler(JChannel ch, MergeView view) {
        this.ch = ch;
        this.view = view;
    }

    public void run() {
        val tmpView = view.getSubgroups().get(0);
        val localAddr = ch.getAddress();
        if (!tmpView.getMembers().contains(localAddr)) {
            System.out.println("Not member of the new primary partition ("
                    + tmpView + "), will re-acquire the state");
            try {
                ch.getState(null, 10000);
                System.out.println("Acquired state.");
            } catch (Exception ignored) {
                System.out.println("Failed acquiring state.");
            }
        } else {
            System.out.println("Member of the new primary partition ("
                    + tmpView + "), will do nothing");
        }
    }
}

