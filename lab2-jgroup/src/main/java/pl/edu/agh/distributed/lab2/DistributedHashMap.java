package pl.edu.agh.distributed.lab2;

import com.google.protobuf.InvalidProtocolBufferException;
import lombok.val;
import org.apache.commons.io.IOUtils;
import org.jgroups.*;
import org.jgroups.stack.ProtocolStack;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

//initial synchronization
//http://www.jgroups.org/manual/index.html#StateTransfer

// meging partitions
//http://www.jgroups.org/manual/index.html#HandlingNetworkPartitions

public class DistributedHashMap extends ReceiverAdapter implements MyHashMap {
    private final JChannel channel;
    private final Map<String, String> concurrentMap;

    public DistributedHashMap(final String channelName, final Integer addressSuffix) throws Exception {
        this.concurrentMap = new ConcurrentHashMap<>();

        ProtocolStack stack = ProtocolStackProvider.prepareProtocolStack(addressSuffix);
        channel = new JChannel(false);

        channel.setProtocolStack(stack);
        stack.init();
        channel.receiver(this);
        channel.connect(channelName);
        channel.getState(null, 10000);
    }

    @Override
    public synchronized void getState(OutputStream output) throws Exception {
        System.out.println("getState in map");
        output.write(ProtoHelpers.mapToProto(concurrentMap).toByteArray());
    }

    @Override
    public synchronized void setState(InputStream input) throws Exception {
        val buff = IOUtils.toByteArray(input);
        val protMap = MapCommands.Map.parseFrom(buff);
        val map = ProtoHelpers.protoMapToDomain(protMap);
        this.concurrentMap.clear();
        System.out.println("setState set in map");
        this.concurrentMap.putAll(map);

    }

    @Override
    public void receive(Message msg) {
        val buffer = msg.getBuffer();
        try {
            val request = MapCommands.Request.parseFrom(buffer);
            if (request.hasPut()) {
                val put = request.getPut();
                concurrentMap.put(put.getKey(), put.getValue());
            } else if (request.hasRemove()) {
                val remove = request.getRemove();
                concurrentMap.remove(remove.getKey());
            }
        } catch (InvalidProtocolBufferException e) {
            System.out.println("received unrecognised message");
        }
    }

    @Override
    public boolean containsKey(String key) {
        return concurrentMap.containsKey(key);
    }

    @Override
    public String get(String key) {
        return concurrentMap.get(key);
    }

    @Override
    public String put(String key, String value) {
        val msg = new Message(null, null, ProtoHelpers.putRequest(key, value).toByteArray());
        val res = concurrentMap.get(key);
        sendByChannel(msg);
        return res;
    }

    @Override
    public String remove(String key) {
        val msg = new Message(null, null, ProtoHelpers.removeRequest(key).toByteArray());
        sendByChannel(msg);
        val res = concurrentMap.get(key);
        sendByChannel(msg);
        return res;
    }

    public void viewAccepted(View view) {
        handleView(channel, view);
    }


    private static void handleView(JChannel ch, View new_view) {
        if (new_view instanceof MergeView) {
            ViewHandler handler = new ViewHandler(ch, (MergeView) new_view);
            handler.start(); // async
        }
    }


    @Override
    public String toString() {
        return "Distributed " + concurrentMap.toString();
    }


    private void sendByChannel(final Message msg) {
        try {
            channel.send(msg);
        } catch (Exception ex) {
            System.out.println("Channel send failed");
            ex.printStackTrace();// msg won't be null
        }
    }
}

