package pl.edu.agh.distributed.lab2;

public interface MyHashMap {

    boolean containsKey(String key);

    String get(String key);

    String put(String key, String value);

    String remove(String key);

}

