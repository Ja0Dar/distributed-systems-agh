package pl.edu.agh.distributed.lab2;

import lombok.val;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        System.setProperty("java.net.preferIPv4Stack", "true");
        val distributedMap = new DistributedHashMap("map", 13);
        val scanner = new Scanner(System.in);
        val controller = new Controller(distributedMap);

        while (true) {
            controller.parseArg(scanner.nextLine());
        }

    }
}
