name := "lab3-rabbitmq"

version := "0.1"

scalaVersion := "2.12.5"

lazy val `lab3-rabbitmq` = project
  .in(file("."))
  .settings(resolvers ++= Dependencies.additionalResolvers)
  .settings(libraryDependencies ++= Dependencies.projectDependencies)
  .settings(scalacOptions ++= CompilerOpts.scalaOptions)
  .settings(PB.targets in Compile := Seq(
    scalapb.gen(flatPackage = true) -> (sourceManaged in Compile).value
  ))
PB.protoSources in Compile := Seq(baseDirectory.value / "src" / "proto")