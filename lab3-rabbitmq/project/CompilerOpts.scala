object CompilerOpts {
  val scalaOptions = Seq(
    "-deprecation",
    "-explaintypes",
    "-Ypartial-unification",
    "-language:higherKinds"
  )
}
