object DependencyVersions {
  val logbackVersion = "1.2.3"
  val slf4jVersion = "1.7.25"
  val scalaLoggingVersion = "3.7.2"
  val scalaTestVersion = "3.0.4"
  val rabbitmqVersion = "5.2.0"

}
