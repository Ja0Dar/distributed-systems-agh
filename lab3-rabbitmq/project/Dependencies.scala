import DependencyVersions._
import sbt._

object Dependencies {
  private val loggingDependencies = Seq(
    "ch.qos.logback"             % "logback-classic"  % logbackVersion,
    "ch.qos.logback"             % "logback-core"     % logbackVersion,
    "org.slf4j"                  % "jcl-over-slf4j"   % slf4jVersion,
    "org.slf4j"                  % "log4j-over-slf4j" % slf4jVersion,
    "org.slf4j"                  % "jul-to-slf4j"     % slf4jVersion,
    "com.typesafe.scala-logging" %% "scala-logging"   % scalaLoggingVersion
  )

  val rabbitDependenceis = Seq(
    "com.rabbitmq" % "amqp-client" % rabbitmqVersion
  )

  private val testDependencies = Seq(
    "org.scalactic"     %% "scalactic"    % scalaTestVersion % Test,
    "org.scalatest"     %% "scalatest"    % scalaTestVersion % Test
  )

  val projectDependencies = Seq(
    loggingDependencies,
    testDependencies,
    rabbitDependenceis
  ).reduce(_ ++ _)

  val additionalResolvers = Seq(
    "Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/"
  )
}
