package pl.edu.agh.distributed.lab3.infrastructure

import java.util.Scanner

import pl.edu.agh.distributed.lab3.messages.Examination
import pl.edu.agh.distributed.lab3.messages.Examination.Unrecognized

import scala.util.Try

trait ComandlineUtils {
  def getExamination(scanner: Scanner): Try[Examination] = Try {
    println("Examination type? 0: knee, 1: hip, 2: elbow")
    val number = Integer.parseInt(scanner.nextLine())
    val result = Examination.fromValue(number)
    if (result.isInstanceOf[Unrecognized]){
      throw new IllegalArgumentException
    }else result
  }.recoverWith { case _: Exception => println("Wrong nr, try again"); getExamination(scanner) }

}
