package pl.edu.agh.distributed.lab3.infrastructure.rabbitApi

import com.rabbitmq.client.{BuiltinExchangeType, Connection}
import pl.edu.agh.distributed.lab3.messages.HospitalMessage

class FanoutWriter(exchangeName: String, connection: Connection) {

  val channel = connection.createChannel()
  channel.exchangeDeclare(exchangeName, BuiltinExchangeType.FANOUT)

  def write(topic: String, message: HospitalMessage): Unit =
    channel.basicPublish(exchangeName, "", null, message.toByteArray)
}
