package pl.edu.agh.distributed.lab3.java_examples

import com.rabbitmq.client.BuiltinExchangeType
import com.rabbitmq.client.Channel
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.Scanner

import com.rabbitmq.client.AMQP.BasicProperties


object Z2_Producer extends App {
  val scanner = new Scanner(System.in)
  // info
  println("Z2 PRODUCER")
  println("Header:")
  val header = scanner.next

  // connection & channel
  val factory = new ConnectionFactory
  factory.setHost("localhost")
  val connection = factory.newConnection
  val channel = connection.createChannel
  // exchange
  val EXCHANGE_NAME = "exchange1"
  channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.HEADERS)


  val headers = Map(header -> "true")

  var message = ""
  while (message != "exit") { // read msg
    val br = new BufferedReader(new InputStreamReader(System.in))
    println("Enter message: ")
    message = br.readLine
    // break condition
    if ("exit" != message) {
      // publish
      channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes("UTF-8"))
      System.out.println("Sent: " + message)
    }
  }
}

