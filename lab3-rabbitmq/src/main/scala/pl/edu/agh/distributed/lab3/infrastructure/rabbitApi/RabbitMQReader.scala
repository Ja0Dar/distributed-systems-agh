package pl.edu.agh.distributed.lab3.infrastructure.rabbitApi

import com.rabbitmq.client._
import pl.edu.agh.distributed.lab3.messages.HospitalMessage

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

abstract class RabbitMQReader(exchangeName: String, connection: Connection, exchangeType: BuiltinExchangeType) {
  val channel = connection.createChannel()

  def queueName: String

  def routingKey: String


  def startFutureReading(callback: HospitalMessage => Unit) = Future(startReading(callback))

  def startReading(callback: HospitalMessage => Unit): Unit = {
    channel.queueBind(queueName, exchangeName, routingKey)
    channel.exchangeDeclare(exchangeName, exchangeType)
    val consumer = new DefaultConsumer(channel) {
      override def handleDelivery(consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties, body: Array[Byte]): Unit = {
        val message = HospitalMessage.parseFrom(body)
        callback(message)
      }
    }
    System.out.println("Waiting for messages...")
    channel.basicConsume(queueName, true, consumer)
  }
}
