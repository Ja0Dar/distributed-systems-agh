package pl.edu.agh.distributed.lab3.java_examples

import java.util.Scanner

import com.rabbitmq.client._


object Z2_Consumer extends App {
  val scanner = new Scanner(System.in)
  System.out.println("Z2 CONSUMER")
  System.out.println("Routing key:")
  val key = scanner.next
  // connection & channel
  val factory = new ConnectionFactory
  factory.setHost("localhost")
  val connection = factory.newConnection
  val channel = connection.createChannel
  // exchange

  val EXCHANGE_NAME = "exchange1"
  channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.HEADERS)
  // queue & bind


  val headers = Map(
    ("x-match", "any"),
    ("First", "A"),
    ("Fourth", "D")
  )

  val queueName = channel.queueDeclare("z2", false, false, false, null).getQueue
  channel.queueBind(queueName, EXCHANGE_NAME, key)
  System.out.println("created queue: " + queueName)
  // consumer (message handling)
  val consumer = new DefaultConsumer(channel) {
    override def handleDelivery(consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties, body: Array[Byte]): Unit = {
      val message = new String(body, "UTF-8")
      System.out.println("Received: " + message)
    }
  }
  // start listening
  System.out.println("Waiting for messages...")
  channel.basicConsume(queueName, true, consumer)
}
