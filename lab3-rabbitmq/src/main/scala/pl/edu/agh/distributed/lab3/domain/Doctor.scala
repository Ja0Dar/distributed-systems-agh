package pl.edu.agh.distributed.lab3.domain

import java.util.{Scanner, UUID}

import pl.edu.agh.distributed.lab3.Config
import pl.edu.agh.distributed.lab3.infrastructure._
import pl.edu.agh.distributed.lab3.infrastructure.rabbitApi.{FanoutReader, TopicReader, TopicWriter}
import pl.edu.agh.distributed.lab3.messages._

object Doctor extends App with HospitalProtoParser with ComandlineUtils {

  val connection = ConnectionProvider.getLocalhostConnection()
  val doctorName = UUID.randomUUID().toString.take(10)

  val topicWriter = new TopicWriter(Config.exchangeName, connection)
  val topicReader = new TopicReader(doctorName, Config.exchangeName, connection)
  val infoReader = new FanoutReader(Config.fanoutExchange, connection)

  topicReader.startFutureReading({ message =>
    val msg = message.msg

    msg.info.map(parseInfo).foreach(println)
    msg.examinationResult.map(parseExaminationResult).foreach(println)

    if (msg.isExamine) {
      throw new IllegalArgumentException(s"Doctor shouldn't receive examination request ${msg.examine.get}")
    }

  })


  infoReader.startFutureReading { hMsg =>
    if (hMsg.msg.isInfo)
      println(s"Received info : ${hMsg.getInfo}")
    else {
      val msg = hMsg.msg
      throw new IllegalArgumentException(s"Technician shouldn't receive this message here: ${msg.examinationResult.orElse(msg.examine).get}")
    }
  }


  val scanner = new Scanner(System.in)

  var patientName = ""
  while (true) {
    println("Patient Name?")
    patientName = scanner.nextLine()
    val examination = getExamination(scanner).get
    val msg = Msg(Examine(patientName, examination, doctorName))
    topicWriter.write(examinationToTopic(examination), msg)
    println(s"Message sent $msg")
  }
}
