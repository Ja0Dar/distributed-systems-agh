package pl.edu.agh.distributed.lab3.infrastructure

import pl.edu.agh.distributed.lab3.messages._

object Msg {
  def apply(info: Info) = HospitalMessage(HospitalMessage.Msg.Info(info))

  def apply(examine: Examine) = HospitalMessage(HospitalMessage.Msg.Examine(examine))

  def apply(examinationResult: ExaminationResult) = HospitalMessage(HospitalMessage.Msg.ExaminationResult(examinationResult))

}
