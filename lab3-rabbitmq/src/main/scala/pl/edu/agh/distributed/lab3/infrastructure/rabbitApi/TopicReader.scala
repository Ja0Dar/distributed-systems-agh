package pl.edu.agh.distributed.lab3.infrastructure.rabbitApi

import com.rabbitmq.client._

class TopicReader(queueTopic: String, exchangeName: String, connection: Connection)
  extends RabbitMQReader(exchangeName, connection, BuiltinExchangeType.TOPIC) {

  val name = queueTopic
  val queueName = channel.queueDeclare(name, false, false, false, null).getQueue
  val routingKey: String = queueTopic
}
