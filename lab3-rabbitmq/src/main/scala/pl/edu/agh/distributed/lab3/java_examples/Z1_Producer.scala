package pl.edu.agh.distributed.lab3.java_examples

import java.util.Scanner

import com.rabbitmq.client.ConnectionFactory


object Z1_Producer extends App {
  System.out.println("Z1 PRODUCER")
  // connection & channel
  val factory = new ConnectionFactory
  factory.setHost("localhost")
  val connection = factory.newConnection
  val channel = connection.createChannel
  // queue
  val QUEUE_NAME = "queue1"
  channel.queueDeclare(QUEUE_NAME, false, false, false, null)
  // producer (publish msg)
  var message = 1
  val scanner = new Scanner(System.in)
  var i = 0
  while ( {
    i < 16
  }) {
    try {
      Thread.sleep(500)
      //                message = scanner.nextInt();
      if (i % 2 == 0) message = 5
      else message = 1
      channel.basicPublish("", QUEUE_NAME, null, message.toString.getBytes)
      System.out.println("Sent: " + message)
    } catch {
      case ex: Exception =>

      //ignore
    }

    {
      i += 1;
      i - 1
    }
  }
  // close
  channel.close()
  connection.close()
}

