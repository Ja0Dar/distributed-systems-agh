package pl.edu.agh.distributed.lab3.infrastructure.rabbitApi

import com.rabbitmq.client._

class FanoutReader(exchangeName: String, connection: Connection)
  extends RabbitMQReader(exchangeName, connection, BuiltinExchangeType.FANOUT) {
  val queueName: String = channel.queueDeclare.getQueue
  val routingKey: String = ""

}
