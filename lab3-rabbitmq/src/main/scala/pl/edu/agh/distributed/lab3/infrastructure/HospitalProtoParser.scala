package pl.edu.agh.distributed.lab3.infrastructure

import pl.edu.agh.distributed.lab3.messages._

trait HospitalProtoParser {
  def examinationToTopic: Examination => String = parseExamination

  def parseExamination(examination: Examination): String =
    examination.toString().toLowerCase()

  def parseInfo(info: Info) =
    s"Admin: ${info.value}"

  def parseExamine: Examine => String = {
    case Examine(patient, examination, _) =>
      s"Examining ${examination.toString().toLowerCase()} for patient $patient"
  }

  def parseExaminationResult: ExaminationResult => String = {
    case ExaminationResult(patient, examination, result) =>
      s"ExaminationRes. Patient: $patient; examination: ${parseExamination(examination)}; result : $result"
  }

}
