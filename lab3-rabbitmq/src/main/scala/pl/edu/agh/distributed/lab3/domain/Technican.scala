package pl.edu.agh.distributed.lab3.domain

import java.util.Scanner

import pl.edu.agh.distributed.lab3.Config
import pl.edu.agh.distributed.lab3.infrastructure._
import pl.edu.agh.distributed.lab3.infrastructure.rabbitApi.{FanoutReader, TopicReader, TopicWriter}
import pl.edu.agh.distributed.lab3.messages._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.language.postfixOps
import concurrent.duration._

object Technican extends App with HospitalProtoParser with ComandlineUtils {
  val connection = ConnectionProvider.getLocalhostConnection()
  val topicWriter = new TopicWriter(Config.exchangeName, connection)

  val scanner = new Scanner(System.in)
  println("First speciality:")
  val firstSpeciality = getExamination(scanner).get

  def getSecondSpeciality: Examination = {
    println("Second speciality:")
    val second = getExamination(scanner).get
    if (second != firstSpeciality) {
      second
    } else {
      println("cannot choose the same")
      getSecondSpeciality
    }
  }

  val secondSpeciality = getSecondSpeciality


  val examineAndRespondToDoctor: HospitalMessage => Unit = { hMsg =>
    val msg = hMsg.msg

    if (msg.isExamine) {
      println(s"Received ${hMsg.getExamine}, responding with OK")
      val Examine(patient, examination, doctor) = msg.examine.get
      topicWriter.write(doctor, Msg(ExaminationResult(patient, examination, "everything is ok")))
    } else {
      throw new IllegalArgumentException(s"Technician shouldn't receive this message here: ${msg.examinationResult.orElse(msg.info).get}")
    }
  }

  val printInfo: HospitalMessage => Unit = { hMsg =>
    print("received info")
    if (hMsg.msg.isInfo)
      println(hMsg.getInfo)
    else {
      val msg = hMsg.msg
      throw new IllegalArgumentException(s"Technician shouldn't receive this message here: ${msg.examinationResult.orElse(msg.examine).get}")
    }
  }

  val firstSpecialityReader = new TopicReader(examinationToTopic(firstSpeciality), Config.exchangeName, connection)
  val secondSpecialityReader = new TopicReader(examinationToTopic(secondSpeciality), Config.exchangeName, connection)
  val infoReader = new FanoutReader(Config.fanoutExchange, connection)


  Await.ready(
    Future.sequence(List(
      firstSpecialityReader.startFutureReading(examineAndRespondToDoctor),
      secondSpecialityReader.startFutureReading(examineAndRespondToDoctor),
      infoReader.startFutureReading(printInfo)))
    , 10 seconds)

}
