package pl.edu.agh.distributed.lab3.domain

import java.util.Scanner

import com.typesafe.scalalogging.StrictLogging
import pl.edu.agh.distributed.lab3.Config
import pl.edu.agh.distributed.lab3.infrastructure._
import pl.edu.agh.distributed.lab3.infrastructure.rabbitApi.{FanoutWriter, TopicReader}
import pl.edu.agh.distributed.lab3.messages._

import scala.language.postfixOps

object Admin extends App with HospitalProtoParser with ComandlineUtils with StrictLogging {

  val connection = ConnectionProvider.getLocalhostConnection()

  val messagesReader = new TopicReader("*", Config.exchangeName, connection)
  val fanoutWriter = new FanoutWriter(Config.fanoutExchange, connection)


  messagesReader.startFutureReading({ hMsg =>
    val msg = hMsg.msg
    msg.examinationResult.orElse(msg.examine).foreach(m => logger.info(s"Logged : $m"))
  })


  val scanner = new Scanner(System.in)
  var infoMessage = Msg(Info(""))
  while (true) {
    println("Message To broadcast:")
    infoMessage = Msg(Info(scanner.nextLine()))
    fanoutWriter.write("info.*", infoMessage)
    println(s"Message sent $infoMessage")
  }
}
