package pl.edu.agh.distributed.lab3.infrastructure.rabbitApi

import com.rabbitmq.client.{BuiltinExchangeType, Connection}
import pl.edu.agh.distributed.lab3.messages.HospitalMessage

class TopicWriter(exchangeName: String, connection: Connection) {

  val channel = connection.createChannel()
  channel.exchangeDeclare(exchangeName, BuiltinExchangeType.TOPIC)

  def write(topic: String, message: HospitalMessage): Unit =
    channel.basicPublish(exchangeName, topic, null, message.toByteArray)
}
