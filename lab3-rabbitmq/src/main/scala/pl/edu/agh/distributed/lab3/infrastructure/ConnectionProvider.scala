package pl.edu.agh.distributed.lab3.infrastructure

import com.rabbitmq.client.{Connection, ConnectionFactory}

object ConnectionProvider {
  def getLocalhostConnection(host: String = "localhost"): Connection= {
    val factory = new ConnectionFactory
    factory.setHost("localhost")
    factory.newConnection
  }
}
