package pl.edu.agh.distributed.lab3.java_examples

import com.rabbitmq.client.{AMQP, ConnectionFactory, DefaultConsumer, Envelope}

object Z1_Consumer extends App{


    // info
    System.out.println("Z1 CONSUMER")

    // connection & channel
    val factory = new ConnectionFactory()
    factory.setHost("localhost")
    val connection = factory.newConnection()
    val channel = connection.createChannel()

    // queue
    val QUEUE_NAME = "queue1"
    channel.queueDeclare(QUEUE_NAME, false, false, false, null)

    // consumer (handle msg)
    val consumer = new DefaultConsumer(channel) {
      override def handleDelivery(consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties, body: Array[Byte]): Unit = {
        val message = new String(body, "UTF-8")
        val intMsg = Integer.parseInt(message)
        System.out.println("Received: " + message + " sleepsing "+ message + " seconds")
        try {
          Thread.sleep(1000*intMsg)
          System.out.println("Sleept enoguh")
          channel.basicAck(envelope.getDeliveryTag, false)
        } catch {
          case e:Exception => e.printStackTrace()
        }
      }
    }

    // start listening
    System.out.println("Waiting for messages...")
    channel.basicQos(1)
    channel.basicConsume(QUEUE_NAME, false, consumer)

    // close
    //        channel.close()
    //        connection.close()
}

