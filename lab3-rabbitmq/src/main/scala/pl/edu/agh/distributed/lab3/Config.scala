package pl.edu.agh.distributed.lab3

object Config {
  val exchangeName = "hospital"
  val fanoutExchange = "hospital-fanout"
}
