#!/bin/bash
rm -rf generated
thrift -r --gen py bank.thrift
mv gen-py generated
rm -rf  gen-py

cd generated/bank
for file in *; do
    cat $file  | sed "s|bank|generated.bank|g" > "$file.back"
    mv "$file.back" $file

done


