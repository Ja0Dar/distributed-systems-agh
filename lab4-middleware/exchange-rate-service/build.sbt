name := "exchange-rate-service"

version := "0.1"

scalaVersion := "2.12.6"



lazy val `exchange-rate-service` = project
  .in(file("."))
  .settings(resolvers ++= Dependencies.additionalResolvers)
  .settings(excludeDependencies ++= Dependencies.globalExcludes)
  .settings(libraryDependencies ++= Dependencies.projectDependencies)
//  .settings(scalacOptions ++= CompilerOpts.scalacOptions)
  .settings(// todo - remove?
    publishArtifact in (Compile, packageDoc) := false,
    publishArtifact in packageDoc := false,
    sources in (Compile, doc) := Seq.empty
  )

PB.protoSources in Compile := Seq(baseDirectory.value / "grpc")

PB.targets in Compile := Seq(
  scalapb.gen(flatPackage = true) -> (sourceManaged in Compile).value
)



