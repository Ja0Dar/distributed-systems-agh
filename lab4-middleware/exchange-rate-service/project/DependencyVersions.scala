object DependencyVersions {
  val logbackVersion       = "1.2.3"
  val slf4jVersion         = "1.7.25"
  val scalaLoggingVersion  = "3.7.2"
  val akkaVersion          = "2.5.8"
  val akkaHttpVersion      = "10.0.11"
  val akkaJson4sVersion    = "1.18.0"
  val json4sVersion        = "3.5.3"
  val scalaTestVersion     = "3.0.4" // todo remove if not used
  val grpcNettyVersion     = com.trueaccord.scalapb.compiler.Version.grpcJavaVersion
  val grpcRuntimeVersion   = com.trueaccord.scalapb.compiler.Version.scalapbVersion
}
