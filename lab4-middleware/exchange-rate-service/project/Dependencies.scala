import DependencyVersions._
import sbt.{SbtExclusionRule, _}

object Dependencies {
  val globalExcludes = Seq(
    SbtExclusionRule("log4j"),
    SbtExclusionRule("log4j2"),
    SbtExclusionRule("commons-logging")
  )

  private val grpcDependencies = Seq(
    "io.grpc" % "grpc-netty" % grpcNettyVersion,
    "com.trueaccord.scalapb" %% "scalapb-runtime-grpc" % grpcRuntimeVersion
  )

  private val loggingDependencies = Seq(
    "ch.qos.logback" % "logback-classic" % logbackVersion,
    "ch.qos.logback" % "logback-core" % logbackVersion,
    "org.slf4j" % "jcl-over-slf4j" % slf4jVersion,
    "org.slf4j" % "log4j-over-slf4j" % slf4jVersion,
    "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingVersion
  )

  private val akkaDependencies = Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
    "com.typesafe.akka" %% "akka-stream" % akkaVersion
  )

  private val akkaHttpDependencies = Seq(// todo remove those if not us
    "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-http-core" % akkaHttpVersion,
    "de.heikoseeberger" %% "akka-http-json4s" % akkaJson4sVersion,
    "org.json4s" %% "json4s-native" % json4sVersion,
    "org.json4s" %% "json4s-core" % json4sVersion
  )

  private val testDependencies = Seq(
    "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test,
    "org.scalatest" %% "scalatest" % "3.0.5" % Test,
    "org.scalamock" %% "scalamock" % "4.1.0" % Test
  )

  val catsDependencies = Seq(
    "org.typelevel" %% "cats-core" % "1.1.0",
    "org.typelevel" % "mouse_2.12" % "0.17", // todo - remove if not used
    "org.typelevel" %% "cats-effect" % "1.0.0-RC" // todo remove if not used
  )

  val projectDependencies: Seq[ModuleID] = Seq(
    loggingDependencies,
    akkaDependencies,
    akkaHttpDependencies,
    testDependencies,
    grpcDependencies,
    catsDependencies
  ).reduce(_ ++ _)

  val additionalResolvers = Seq(
    "Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/"
  )
}
