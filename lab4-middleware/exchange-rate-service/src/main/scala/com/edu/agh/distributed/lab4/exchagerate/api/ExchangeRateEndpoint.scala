package com.edu.agh.distributed.lab4.exchagerate.api

import java.util.concurrent.{ConcurrentHashMap, Executors, TimeUnit}

import com.edu.agh.distributed.lab4.exchange_rates.ExchangeRateServiceGrpc.ExchangeRateService
import com.edu.agh.distributed.lab4.exchange_rates.{Currency, CurrencyAndRate, ExchangeRatesRequest, ExchangeRatesResponse}
import com.typesafe.scalalogging.StrictLogging
import io.grpc.stub.StreamObserver

import scala.collection.JavaConverters._
import scala.util.Random

class ExchangeRateEndpoint extends ExchangeRateService with StrictLogging {
  var rates: Map[Currency, Double] = Currency.values.map(_ -> Random.nextDouble() * 3).toMap

  private var observerToInterestedCurrencies = new ConcurrentHashMap[StreamObserver[ExchangeRatesResponse], List[Currency]]().asScala
  private val scheduler = Executors.newScheduledThreadPool(1)
  scheduler.scheduleAtFixedRate(() => changeRateAndNotify(), 350, 200, TimeUnit.MILLISECONDS)

  override def getExchangeRates(request: ExchangeRatesRequest, responseObserver: StreamObserver[ExchangeRatesResponse]): Unit = {
    logger.info(s"Exchange rate received request $request")
    observerToInterestedCurrencies += (responseObserver -> request.interesingCurrencies.toList)
    rates.filterKeys(request.interesingCurrencies.contains(_)).foreach { case (currency, rate) => responseObserver.onNext(ExchangeRatesResponse(Seq(CurrencyAndRate(currency, rate)))) }
  }


  def changeRateAndNotify(): Unit = {
    val currencyToChange = Currency.values(Random.nextInt(Currency.values.length))
    val newRate = rates(currencyToChange) * (1.28 - Random.nextDouble() * 0.4)
    logger.info(s"Changed $currencyToChange rate to $newRate")

    rates += (currencyToChange -> newRate)

    observerToInterestedCurrencies.filter { case (_, currencies) => currencies.contains(currencyToChange) }.keys.foreach { observer =>
      val message = ExchangeRatesResponse(Seq(CurrencyAndRate(currencyToChange, newRate)))
      logger.info(s"Sending message $message")
      try {
        observer.onNext(message)
      } catch {
        case ex => observerToInterestedCurrencies -= observer
          logger.error("Removing observer due to error. ( Probably bank disconnected)", ex)
      }

    }

  }

}
