package com.edu.agh.distributed.lab4.exchagerate

import com.edu.agh.distributed.lab4.exchagerate.api.ExchangeRateEndpoint
import com.edu.agh.distributed.lab4.exchange_rates.ExchangeRateServiceGrpc
import com.typesafe.scalalogging.StrictLogging
import io.grpc.ServerBuilder

import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.Implicits.global

object Main extends App with StrictLogging {

  val port = 8080

  val service = new ExchangeRateEndpoint()

  val server = ServerBuilder
    .forPort(port)
    .addService(ExchangeRateServiceGrpc.bindService(service, implicitly[ExecutionContext]))
    .build()

  server.start()
  logger.info(s"API STARTED ON PORT: ${server.getPort}")
   server.awaitTermination()
}
