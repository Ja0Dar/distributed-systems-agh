#!/bin/bash
cp bank.thrift ../bank/src/main/thrift
cp bank.thrift ../client
cp exchange_rate.proto ../bank/grpc/exchange_rate.proto
cp exchange_rate.proto ../exchange-rate-service/grpc/exchange_rate.proto
