#@namespace scala com.edu.agh.distributed.lab4.sharedmodel.thrift
namespace java com.edu.agh.distributed.lab4.sharedmodel.thrift

enum CreateAccountError{
    PESEL_ALREADY_IN_USE = 0,
    NEGATIVE_INCOME= 1
}

union CreateAccountResponse {
  1: string key;
  2: list<CreateAccountError> errors;
}


service AccountCreatingService{
  CreateAccountResponse createAccount(1: required string pesel, 2: required i64 declaredIncomeInEuroCents)
}

enum GetBalanceError {
    KEY_IS_INVALID = 0,
    PESEL_NOT_FOUND =1
    ACCOUNT_NOT_PREMIUM =2
}

union GetBalanceResponse{
  1: i64 balanceInEuroCents;
  2: list<GetBalanceError> errors;
}

service StandardAccountService{
    GetBalanceResponse getBalance(1: string pesel,2: string accountKey)

}
enum Currency {
    USD = 0,
    PLN = 1,
    CHF = 2,
    BC = 3,
    GBP = 4,
    JOD = 5,
    JPY = 6,
    KES = 7,
    KGS = 8,
    KHR = 9,
    LSL = 10
}

struct LoanOffer{
    1: Currency quotedCurrency;
    2: i64 time
    3: i64 costInQuotedCurrencyCents;
    4: i64 constInEuroCents;
}

enum GetLoanOffersError{
    KEY_IS_INVALID = 0,
    PESEL_NOT_FOUND =1,
    ACCOUNT_NOT_PREMIUM = 2;
    NEGATIVE_VOLUME =3;
}

union GetLoanOffersResponse{
    1: list<LoanOffer> offers;
    2: list<GetLoanOffersError> errors;
    3: list<Currency> availableCurrencies;
}

service PremiumAccountService extends StandardAccountService{
    GetLoanOffersResponse getLoanOffers(1:string pesel, 2:string accountKey,3: Currency currency,4: i64 volumeInQuotedCurrency)
}
