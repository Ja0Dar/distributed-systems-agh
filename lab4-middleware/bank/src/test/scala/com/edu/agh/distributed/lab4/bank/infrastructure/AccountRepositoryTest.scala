package com.edu.agh.distributed.lab4.bank.infrastructure

import cats.data.NonEmptyList
import com.edu.agh.distributed.lab4.bank.domain.AccountService
import com.edu.agh.distributed.lab4.bank.domain.Defs.Account
import com.edu.agh.distributed.lab4.sharedmodel.thrift.CreateAccountError.{NegativeIncome, PeselAlreadyInUse}
import com.edu.agh.distributed.lab4.sharedmodel.thrift.{Currency, GetBalanceError, GetLoanOffersError, LoanOffer}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}

import scala.language.postfixOps

class AccountRepositoryTest extends FlatSpec with Matchers with BeforeAndAfterEach with MockFactory {

  val premiumAccountThreshold = 10000L
  var accountRepository: AccountService = _
  var exchangeRateService: ExchangeRateService = _

  "createAccount" should "return key if declared income is > 0 and pesel wasn't used before " in {
    val pesel = "31143"
    accountRepository.createAccount(pesel, 100) should matchPattern { case Right(Account(`pesel`, _, _, _)) => }
  }

  it should "return PeselAlreadyInUse if pesel was used before" in {
    val pesel = "31143"
    accountRepository.createAccount(pesel, 100)

    accountRepository.createAccount(pesel, 100) should matchPattern { case Left(NonEmptyList(PeselAlreadyInUse, Nil)) => }
  }

  it should "return incomeIsNegative if declared in come was negative" in {
    val pesel = "31143"
    accountRepository.createAccount(pesel, -100L).left.get.toList should contain(NegativeIncome)
  }

  it should "add account" in {
    val pesel = "31143"
    val key = accountRepository.createAccount(pesel, 100L).right.get.accountKey
    accountRepository.getAccount(pesel, key) should matchPattern { case Right(_) => }
  }

  "getAccount" should "return pesel not exists if pesel doesn't exist" in {
    val pesel = "31143"
    accountRepository.getAccount(pesel, "") shouldBe Left(GetBalanceError.PeselNotFound)
  }

  it should "return InvalidKey if key is invalid " in {
    val pesel = "31143"
    accountRepository.createAccount(pesel, 100L).right.get.accountKey
    accountRepository.getAccount(pesel, "invalid") shouldBe Left(GetBalanceError.KeyIsInvalid)
  }

  "getOffers" should "return pesel not exists if pesel doesn't exist" in {
    val pesel = "31143"
    accountRepository.getLoanOffers(pesel, "", Currency.Jod, 1L) shouldBe Left(GetLoanOffersError.PeselNotFound)
  }

  it should "return InvalidKey if key is invalid " in {
    val pesel = "31143"
    accountRepository.createAccount(pesel, 100L).right.get.accountKey
    accountRepository.getLoanOffers(pesel, "invalid", Currency.Jod, 4L) shouldBe Left(GetLoanOffersError.KeyIsInvalid)
  }

  it should "return AccountNotPremium if account isn't premium" in {
    val pesel = "31143"
    val key = accountRepository.createAccount(pesel, premiumAccountThreshold - 3L).right.get.accountKey
    accountRepository.getLoanOffers(pesel, key, Currency.Jod, 5L) shouldBe Left(GetLoanOffersError.AccountNotPremium)
  }

  it should "return volume is negative if loan volume is not positive" in {
    val pesel = "31143"
    val key = accountRepository.createAccount(pesel, premiumAccountThreshold + 3L).right.get.accountKey
    accountRepository.getLoanOffers(pesel, key, Currency.Jod, -5L) shouldBe Left(GetLoanOffersError.NegativeVolume)
  }

  it should "return some offers if everything is ok" in {
    val pesel = "31143"
    val key = accountRepository.createAccount(pesel, premiumAccountThreshold + 3L).right.get.accountKey
    val time = 5L
    val loanOffers = NonEmptyList(
      LoanOffer(Currency.Jod, 100L, 0, 0),
      List(LoanOffer(Currency.Jod, 200L, 0, 0),
        LoanOffer(Currency.Jod, 300L, 0, 0))
    )

    (exchangeRateService.getExchangeRate _).expects(Currency.Jod).returning(Some(0))

    accountRepository.getLoanOffers(pesel, key, Currency.Jod, time) shouldBe Right(Some(loanOffers))
  }

  override protected def beforeEach(): Unit = {
    exchangeRateService = mock[ExchangeRateService]
    accountRepository = new AccountService(premiumAccountThreshold, exchangeRateService = exchangeRateService)
  }
}
