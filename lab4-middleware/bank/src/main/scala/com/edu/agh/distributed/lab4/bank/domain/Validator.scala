package com.edu.agh.distributed.lab4.bank.domain

import cats.data.{NonEmptyList, Validated}
import cats.implicits._
import com.edu.agh.distributed.lab4.sharedmodel.thrift.{CreateAccountError, GetBalanceError}

import scala.language.implicitConversions

object Validator {

  type CreateAccountValidationResult[T] = Validated[NonEmptyList[CreateAccountError], T]
  type GetBalanceValidationResult[T] = Validated[NonEmptyList[GetBalanceError], T]

  implicit def validateCreateAccount[T](tup: (T, T => Boolean, CreateAccountError)): CreateAccountValidationResult[T] = {
    val (entity, validator, error) = tup
    if (validator(entity)) {
      entity.validNel
    } else {
      error.invalidNel
    }
  }

  implicit def validateGetBalance[T](tup: (T, T => Boolean, GetBalanceError)): GetBalanceValidationResult[T] = {
    val (entity, validator, error) = tup
    if (validator(entity)) {
      entity.validNel
    } else {
      error.invalidNel
    }
  }

}
