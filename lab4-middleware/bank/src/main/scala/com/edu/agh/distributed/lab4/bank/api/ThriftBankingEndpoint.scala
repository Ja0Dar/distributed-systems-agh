package com.edu.agh.distributed.lab4.bank.api

import com.edu.agh.distributed.lab4.bank.domain.AccountService
import com.edu.agh.distributed.lab4.bank.domain.Defs.AccountType
import com.edu.agh.distributed.lab4.sharedmodel.thrift._
import com.twitter.finagle.ThriftMux
import com.twitter.util.{Await, Future}
import com.typesafe.scalalogging.StrictLogging
import org.apache.thrift.protocol.TBinaryProtocol

class ThriftBankingEndpoint(accountService: AccountService) extends StrictLogging {
  def run(port: Int) {

    val accountCreatingService = new AccountCreatingService[Future] {
      override def createAccount(pesel: String, declaredIncomeInCents: Long): Future[CreateAccountResponse] = Future {
        logger.info(s"Received CreateAccount(pesel=$pesel,income=$declaredIncomeInCents)")
        val res = accountService.createAccount(pesel, declaredIncomeInCents) match {
          case Right(account) => CreateAccountResponse.Key(account.accountKey)
          case Left(errors) => CreateAccountResponse.Errors(errors.toList)
        }
        logger.info(s"Returning $res in accountCreating service")
        res
      }
    }

    val standardService = new StandardAccountService[Future] {
      override def getBalance(pesel: String, accountKey: String): Future[GetBalanceResponse] = Future {
        logger.info(s"Received StandardGetBalance(pesel=$pesel,key=$accountKey)")
        accountService.getAccount(pesel, accountKey) match {
          case Right(account) => GetBalanceResponse.BalanceInEuroCents(account.balanceInEuroCents)
          case Left(error) => GetBalanceResponse.Errors(error :: Nil)
        }
      }
    }

    val premiumService = new PremiumAccountService[Future] {
      override def getBalance(pesel: String, accountKey: String): Future[GetBalanceResponse] = Future {
        logger.info(s"Received PremiumGetBalance(pesel=$pesel,key=$accountKey)")
        accountService.getAccount(pesel, accountKey) match {
          case Right(account) if account.accountType == AccountType.Premium => GetBalanceResponse.BalanceInEuroCents(account.balanceInEuroCents)
          case Right(_) => GetBalanceResponse.Errors(GetBalanceError.AccountNotPremium :: Nil)
          case Left(error) => GetBalanceResponse.Errors(error :: Nil)
        }
      }

      override def getLoanOffers(pesel: String, accountKey: String, currency: Currency, volumeInQuotedCurrency: Long): Future[GetLoanOffersResponse]
      = Future {
        logger.info(s"Received GetLoanOffers(pesel=$pesel,key=$accountKey,currency=$currency,volume=$volumeInQuotedCurrency)")
        accountService.getLoanOffers(pesel, accountKey, currency, volumeInQuotedCurrency) match {
          case Right(Some(offers)) => GetLoanOffersResponse.Offers(offers.toList)
          case Right(None) => GetLoanOffersResponse.AvailableCurrencies(accountService.availableCurrencies.toList)
          case Left(error) => GetLoanOffersResponse.Errors(error :: Nil)
        }
      }

    }

    val server = ThriftMux.server
      .withProtocolFactory(new TBinaryProtocol.Factory).serveIfaces(s"localhost:$port",
      Map(
        "accountCreatingService" -> accountCreatingService,
        "standardAccountService" -> standardService,
        "premiumAccountService" -> premiumService
      )
    )
    println(s"Start thrift server on $port port...")
    Await.result(server)
  }
}


