package com.edu.agh.distributed.lab4.bank

import com.edu.agh.distributed.lab4.bank.api.ThriftBankingEndpoint
import com.edu.agh.distributed.lab4.bank.domain.AccountService
import com.edu.agh.distributed.lab4.bank.infrastructure.ExchangeRateService
import com.edu.agh.distributed.lab4.sharedmodel.thrift.Currency
import com.typesafe.scalalogging.StrictLogging

import scala.util.{Random, Try}

object Main extends App with StrictLogging {

  override def main(args: Array[String]): Unit = {
    val port = args.headOption.flatMap(arg => Try(arg.toInt).toOption).getOrElse(1234)


    logger.info(s"Starting service on port $port")
    val currencies: List[Currency] = (1 to 5).map(_ => Random.nextInt(Currency.list.length)).map(Currency.list(_)).toList


    val bankingService = new ThriftBankingEndpoint(new AccountService(100, new ExchangeRateService(currencies, "localhost", 8080)))
    bankingService.run(port)
  }


}
