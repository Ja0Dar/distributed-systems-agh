package com.edu.agh.distributed.lab4.bank.domain

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.locks.ReentrantLock

import cats.data.{NonEmptyList => NEL}
import cats.implicits._
import com.edu.agh.distributed.lab4.bank.domain.Defs.AccountType.Premium
import com.edu.agh.distributed.lab4.bank.domain.Defs.{Account, AccountKey, AccountType, Pesel}
import com.edu.agh.distributed.lab4.bank.domain.Validator._
import com.edu.agh.distributed.lab4.bank.infrastructure.ExchangeRateService
import com.edu.agh.distributed.lab4.sharedmodel.thrift._
import mouse.boolean._

import scala.collection.JavaConverters._
import scala.collection.concurrent
import scala.util.Random


class AccountService(premiumThreshold: Long, exchangeRateService: ExchangeRateService) {
  private val accounts: concurrent.Map[Pesel, Account] = new ConcurrentHashMap[Pesel, Account]().asScala
  private val accountLock = new ReentrantLock()
  private val availableLoanTimes = List(6, 12, 24)
  private val maxTime = availableLoanTimes.reduce(Math.max)

  def createAccount(pesel: Pesel, incomeInEuroCents: Long): Either[NEL[CreateAccountError], Account] = locked(accountLock) {
    (
      validateCreateAccount[Pesel](pesel, p => !accounts.keySet.contains(p), CreateAccountError.PeselAlreadyInUse),
      validateCreateAccount[Long](incomeInEuroCents, _ >= 0, CreateAccountError.NegativeIncome)
    ).mapN[Account] { (validPesel: Pesel, validIncome: Long) =>

      Account(
        pesel = validPesel,
        accountType = if (validIncome >= premiumThreshold) AccountType.Premium else AccountType.Standard,
        accountKey = Random.alphanumeric.take(4).mkString.toLowerCase(),
        balanceInEuroCents = Math.abs(Random.nextLong() % 1000000L)
      )

    }.map((account: Account) => {
      accounts.put(account.pesel, account)
      account
    }).toEither
  }

  def getAccount(pesel: Pesel, key: AccountKey): Either[GetBalanceError, Account] = locked(accountLock) {
    for {
      account <- accounts.get(pesel).toRight(GetBalanceError.PeselNotFound)
      _ <- key == account.accountKey either(GetBalanceError.KeyIsInvalid, ())
    } yield account
  }

  def getLoanOffers(pesel: Pesel, key: AccountKey, currency: Currency, volume: Long): Either[GetLoanOffersError, Option[NEL[LoanOffer]]] = locked(accountLock) {
    for {
      account <- accounts.get(pesel).toRight(GetLoanOffersError.PeselNotFound)
      _ <- key == account.accountKey either(GetLoanOffersError.KeyIsInvalid, ())
      _ <- account.accountType == Premium either(GetLoanOffersError.AccountNotPremium, ())
      _ <- volume > 0L either(GetLoanOffersError.NegativeVolume, ())
    } yield exchangeRateService.getExchangeRate(currency).map { exchangeRate =>
      NEL(availableLoanTimes.head, availableLoanTimes.tail).map { time =>
        val timeFactor: Double = 2 - time.toDouble / maxTime.toDouble

        LoanOffer(currency, time, (volume.toDouble * timeFactor).toLong, (volume * exchangeRate * timeFactor).toLong)
      }
    }
  }

  def availableCurrencies: Set[Currency] = exchangeRateService.getAvailableCurrencies

  def locked[T](lock: ReentrantLock)(t: => T): T = {
    lock.lock()
    val result: T = t
    lock.unlock()
    result
  }
}
