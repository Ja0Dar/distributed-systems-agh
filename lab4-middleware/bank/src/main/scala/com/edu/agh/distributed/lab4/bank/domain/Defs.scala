package com.edu.agh.distributed.lab4.bank.domain

object Defs {

  type Pesel = String
  type AccountKey = String

  case class Account(pesel: Pesel, accountType: AccountType, accountKey: AccountKey, balanceInEuroCents: Long)

  sealed trait AccountType

  object AccountType {

    case object Premium extends AccountType

    case object Standard extends AccountType

  }

}
