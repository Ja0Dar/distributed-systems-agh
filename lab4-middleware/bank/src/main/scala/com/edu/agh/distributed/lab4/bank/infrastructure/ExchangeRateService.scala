package com.edu.agh.distributed.lab4.bank.infrastructure


import java.util.concurrent.ConcurrentHashMap

import com.edu.agh.distributed.lab4.exchange_rates
import com.edu.agh.distributed.lab4.exchange_rates.{CurrencyAndRate, ExchangeRateServiceGrpc, ExchangeRatesRequest, ExchangeRatesResponse}
import com.edu.agh.distributed.lab4.sharedmodel.thrift
import com.edu.agh.distributed.lab4.sharedmodel.thrift.Currency
import com.typesafe.scalalogging.StrictLogging
import io.grpc.ManagedChannelBuilder
import io.grpc.stub.StreamObserver

import scala.collection.JavaConverters._
import scala.collection.concurrent


class ExchangeRateService(interestedRates: List[thrift.Currency], host: String, port: Int) extends StrictLogging {

  private val channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext(true).build()
  private val stub = ExchangeRateServiceGrpc.stub(channel)

  val cachedRates: concurrent.Map[Currency, Double] = new ConcurrentHashMap[thrift.Currency, Double]().asScala

  private val responseObserver = new StreamObserver[ExchangeRatesResponse]() {
    override def onNext(response: ExchangeRatesResponse): Unit = {
      response.rates.foreach { case CurrencyAndRate(currency, rate) =>
        logger.info(s"Received exchange rate : ${currency.toString}, $rate")
        cachedRates += (currencyProtoToThrift(currency) -> rate)
      }
    }

    override def onError(t: Throwable): Unit = {
      logger.error("Oh no, stream error", t)
    }

    override def onCompleted(): Unit = {
      logger.info("End of stream")
    }
  }

  stub.getExchangeRates(ExchangeRatesRequest(interestedRates.map(currencyThriftToProto)), responseObserver)


  def getAvailableCurrencies = interestedRates.toSet

  def getExchangeRate(currency: thrift.Currency): Option[Double] =
    cachedRates.get(currency)

  def currencyThriftToProto: thrift.Currency => exchange_rates.Currency = {
    case thrift.Currency.Usd => exchange_rates.Currency.USD
    case thrift.Currency.Pln => exchange_rates.Currency.PLN
    case thrift.Currency.Chf => exchange_rates.Currency.CHF
    case thrift.Currency.Bc => exchange_rates.Currency.BC
    case thrift.Currency.Gbp => exchange_rates.Currency.GBP
    case thrift.Currency.Jod => exchange_rates.Currency.JOD
    case thrift.Currency.Jpy => exchange_rates.Currency.JPY
    case thrift.Currency.Kes => exchange_rates.Currency.KES
    case thrift.Currency.Kgs => exchange_rates.Currency.KGS
    case thrift.Currency.Khr => exchange_rates.Currency.KHR
    case thrift.Currency.Lsl => exchange_rates.Currency.LSL
    case thrift.Currency.EnumUnknownCurrency(id) =>
      logger.error(s"Unrecognised thrift currency $id")
      exchange_rates.Currency.Unrecognized(id)
  }

  def currencyProtoToThrift: exchange_rates.Currency => thrift.Currency = {
    case exchange_rates.Currency.USD => thrift.Currency.Usd
    case exchange_rates.Currency.PLN => thrift.Currency.Pln
    case exchange_rates.Currency.CHF => thrift.Currency.Chf
    case exchange_rates.Currency.BC => thrift.Currency.Bc
    case exchange_rates.Currency.GBP => thrift.Currency.Gbp
    case exchange_rates.Currency.JOD => thrift.Currency.Jod
    case exchange_rates.Currency.JPY => thrift.Currency.Jpy
    case exchange_rates.Currency.KES => thrift.Currency.Kes
    case exchange_rates.Currency.KGS => thrift.Currency.Kgs
    case exchange_rates.Currency.KHR => thrift.Currency.Khr
    case exchange_rates.Currency.LSL => thrift.Currency.Lsl
    case exchange_rates.Currency.Unrecognized(id) =>
      logger.error(s"Unrecognised proto currency $id")
      thrift.Currency.EnumUnknownCurrency(id)
  }
}

