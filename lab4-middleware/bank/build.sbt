name := "bank"

version := "0.1"

scalaVersion := "2.12.6"


val protocDependencies = Seq(
  "com.trueaccord.scalapb" %% "scalapb-runtime"      % com.trueaccord.scalapb.compiler.Version.scalapbVersion % "protobuf",
  // for gRPC
  "io.grpc"                %  "grpc-netty"           % "1.4.0",
  "com.trueaccord.scalapb" %% "scalapb-runtime-grpc" % com.trueaccord.scalapb.compiler.Version.scalapbVersion,
  // for JSON conversion
  "com.trueaccord.scalapb" %% "scalapb-json4s"       % "0.3.0"
)


resolvers += Resolver.bintrayRepo("beyondthelines", "maven")

val monocleVersion = "1.5.0" // 1.5.0-cats based on cats 1.0.x
val monocleDependencies = Seq( // todo remove if not used
  "com.github.julien-truffaut" %% "monocle-core" % monocleVersion,
  "com.github.julien-truffaut" %% "monocle-macro" % monocleVersion,
  "com.github.julien-truffaut" %% "monocle-law" % monocleVersion % "test"
)

val scroogeVersion = "18.4.0"
val thriftDependencies = Seq(
  "com.twitter" %% "scrooge-core" % scroogeVersion exclude("com.twitter", "libthrift"),
  "com.twitter" %% "finagle-thrift" % scroogeVersion exclude("com.twitter", "libthrift"),
  "com.twitter" %% "finagle-thriftmux" % scroogeVersion exclude("com.twitter", "libthrift"),
  "org.apache.thrift" % "libthrift" % "0.10.0" // pomOnly() // todo - or 0.11.0
)

val catsDependencies = Seq(
  "org.typelevel" %% "cats-core" % "1.1.0",
  "org.typelevel" % "mouse_2.12" % "0.17", // todo - remove if not used
  "org.typelevel" %% "cats-effect" % "1.0.0-RC" // todo remove if not used
)

val logbackVersion       = "1.2.3"
val slf4jVersion         = "1.7.25"
val scalaLoggingVersion  = "3.7.2"

val loggingDependencies = Seq(
  "ch.qos.logback" % "logback-classic" % logbackVersion,
  "ch.qos.logback" % "logback-core" % logbackVersion,
  "org.slf4j" % "jcl-over-slf4j" % slf4jVersion,
  "org.slf4j" % "log4j-over-slf4j" % slf4jVersion,
  "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingVersion
)

val testDependencies = Seq(
  "org.scalatest" %% "scalatest" % "3.0.5" % Test,
  "org.scalamock" %% "scalamock" % "4.1.0" % Test
)

libraryDependencies ++= thriftDependencies ++ catsDependencies ++ monocleDependencies ++ testDependencies ++ loggingDependencies ++ protocDependencies


PB.protoSources in Compile := Seq(baseDirectory.value / "grpc")

PB.targets in Compile := Seq(
  scalapb.gen(flatPackage = true) -> (sourceManaged in Compile).value
)
