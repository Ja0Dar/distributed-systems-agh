name := "lab6-zookeeper"

version := "0.1"

scalaVersion := "2.12.6"

libraryDependencies += "org.apache.zookeeper" % "zookeeper" % "3.4.12"
