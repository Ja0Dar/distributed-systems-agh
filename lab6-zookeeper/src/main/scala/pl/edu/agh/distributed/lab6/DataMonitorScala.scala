package pl.edu.agh.distributed.lab6

import org.apache.zookeeper.KeeperException.Code
import org.apache.zookeeper.Watcher.Event
import org.apache.zookeeper.Watcher.Event.KeeperState._
import org.apache.zookeeper._
import org.apache.zookeeper.data.Stat
import scala.collection.JavaConverters._

object DataMonitorScala {

  trait DataMonitorListener {
    def existStatusChanged(data: Array[Byte]): Unit

    def closing(reasonCode: Int): Unit
  }

}

class DataMonitorScala(zk: ZooKeeper, znode: String, chainedWatcher: Watcher, var listener: DataMonitorScala.DataMonitorListener) extends Watcher with AsyncCallback.StatCallback {
  // to be completely event driven
  zk.exists(znode, true, this, null)

  var dead = false
  var prevData: Array[Byte] = Array.emptyByteArray

  override def process(event: WatchedEvent): Unit = {
    event.getType match {
      case Event.EventType.None if event.getState == Expired =>
        dead = true
        listener.closing(KeeperException.Code.SESSIONEXPIRED.intValue())

      case _ if Option(event.getPath).contains(znode) =>
        zk.exists(znode, true, this, null)
        watchChildren(znode)

      case Event.EventType.NodeChildrenChanged =>
        zk.exists(znode, true, this, null)
        watchChildren(znode)


    }

    if (chainedWatcher != null) chainedWatcher.process(event)
  }

  override def processResult(rc: Int, path: String, ctx: Any, stat: Stat): Unit = {
    val code = Code.get(rc)

    code match {
      case Code.OK =>
        val dataBytes = Option(zk.getData(znode, false, null)).getOrElse(Array.emptyByteArray)
        println(s"Number of children :${getChildren(znode)}, Triggering path: $path")

        if (dataBytes.isEmpty ^ prevData.isEmpty) {
          listener.existStatusChanged(dataBytes)
          prevData = dataBytes
        }
      case Code.NONODE =>
        listener.existStatusChanged(Array.emptyByteArray)
        prevData = Array.emptyByteArray
      case Code.SESSIONEXPIRED | Code.NOAUTH =>
        dead = true
        listener.closing(rc)
      case _ =>
        zk.exists(znode, true, this, null)
        watchChildren(znode)
    }
  }


  def watchChildren(node: String): Unit = {
    val childern = zk.getChildren(node, true)
    childern.forEach { chld =>
      val chldPath = s"$node/$chld"
      watchChildren(chldPath)
    }
  }

  def getChildren(node: String): Int = {
    val childern = zk.getChildren(node, true).asScala

    childern.map { chld =>
      val chldPath = s"$node/$chld"
      getChildren(chldPath)
    }
      .fold(childern.size)(_ + _)
  }
}

