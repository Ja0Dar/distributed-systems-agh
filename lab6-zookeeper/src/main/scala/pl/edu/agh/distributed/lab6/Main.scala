package pl.edu.agh.distributed.lab6

object Main extends App {
  if (args.length < 4) {
    System.err.println("USAGE: Executor hostPort znode filename program [args ...]")
    System.exit(2)
  }
  val hostPort = args(0)
  val znode = args(1)
  val filename = args(2)
  val exec = new Array[String](args.length - 3)

  System.arraycopy(args, 3, exec, 0, exec.length)

  new ExecutorScala(hostPort, znode, filename, exec).run()
}
