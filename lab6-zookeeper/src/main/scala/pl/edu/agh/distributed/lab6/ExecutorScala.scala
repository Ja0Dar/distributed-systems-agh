package pl.edu.agh.distributed.lab6

import java.io.{FileOutputStream, InputStream, OutputStream}
import java.util.Scanner

import org.apache.zookeeper.{KeeperException, WatchedEvent, Watcher, ZooKeeper}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


object ExecutorScala {

  def writeStreams(is: InputStream, os: OutputStream) = Future {
    val b = new Array[Byte](80)
    var rc = is.read(b)
    while (rc > 0) {
      os.write(b, 0, rc)
      rc = is.read(b)
    }
  }
}

class ExecutorScala(
  val hostPort: String,
  znode: String,
  filename: String,
  exec: Array[String])
  extends Watcher
    with Runnable
    with DataMonitorScala.DataMonitorListener {

  val zk = new ZooKeeper(hostPort, 3000, this)
  val dm = new DataMonitorScala(zk, znode, null, this)
  private var child: Option[Process] = None


  override def process(event: WatchedEvent): Unit =
    dm.process(event)


  override def run(): Unit = {
    prompt
    synchronized {
      while (!dm.dead) wait()
    }
  }

  override def closing(rc: Int): Unit = {
    this synchronized notifyAll()
  }

  def prompt: Future[Unit] = Future {
    val scanner = new Scanner(System.in)
    println(s"Print sth to see $znode tree, `exit` to quit")
    while (scanner.next() != "exit") {
      printChildren(znode)
      println(s"Print sth to see $znode tree, `exit` to quit")
    }

    println("Exiting")

  }.recoverWith {
    case ex: KeeperException.NoNodeException =>
      println(s"Cannot print tree, $znode absent")
      prompt
    case ex =>
      ex.printStackTrace()
      throw ex
  }

  def printChildren(node: String): Unit = {
    val childern = zk.getChildren(node, true)
    println(node)
    childern.forEach { chld =>
      val chldPath = s"$node/$chld"
      printChildren(chldPath)
    }
  }


  override def existStatusChanged(data: Array[Byte]): Unit = {
    if (data sameElements Array.emptyByteArray) {
      child.foreach { chld =>
        System.out.println("Killing child process")
        chld.destroy()
        chld.waitFor()
        child = None
      }
    } else {
      child.foreach { chld =>
        System.out.println("Stopping child if exists")
        chld.destroy()
        chld.waitFor()
      }

      val fos = new FileOutputStream(filename)
      fos.write(data)
      fos.close()
      System.out.println("Starting child process")
      child = Some(Runtime.getRuntime.exec(exec))
      ExecutorScala.writeStreams(child.get.getInputStream, System.out)
      ExecutorScala.writeStreams(child.get.getErrorStream, System.err)
    }
  }

}


